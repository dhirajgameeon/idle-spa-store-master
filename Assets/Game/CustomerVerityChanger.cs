using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerVerityChanger : MonoBehaviour
{
    public bool isMale = false;
    public int Num;
    public SkinnedMeshRenderer SMRMain;    
    public SkinnedMeshRenderer SMRGrown;    
    public SkinnedMeshRenderer SMRProcessing;    
    public Texture[] TT;

    private void OnEnable()
    {

        if (!isMale)
        {
            SMRMain.material.mainTexture = TT[Num];
            SMRProcessing.material.mainTexture = TT[Num];
            foreach(var mat in SMRGrown.materials)
            {
                mat.mainTexture = TT[Num];
            }
        }
        
        if (isMale)
        {
            Texture t = TT[Num];
            foreach (var item in SMRMain.materials)
            {
                item.mainTexture = t;
            }
            foreach (var item in SMRProcessing.materials)
            {
                item.mainTexture = t;
            }
            SMRGrown.material.mainTexture = t;
        }
    }

}
