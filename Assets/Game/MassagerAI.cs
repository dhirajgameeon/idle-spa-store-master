using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MassagerAI : MonoBehaviour
{
    private moveEmp MP;
    public animationHandler AH;
    public Bed BED;
    public float StartTimer = 0.15f;
    float StartTime = 0;



    public bool isProcessStart;
    private void Awake()
    {
        MP = GetComponent<moveEmp>();
        StartTime = StartTimer;
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        MassageProcess();
    }

    public void MassageProcess()
    {
        if (!isProcessStart && BED && BED.isPlayerNear && BED.isCustomerNear && !BED.waitTimer.isWaitTimerComplete)
        {
            MP.agent.isStopped = true;
            MassgeStart();
        }
        if (isProcessStart && BED.waitTimer.isWaitTimerComplete)
        {

            AH.anim.SetTrigger("ME");
            StartTime = StartTimer;
            MP.agent.isStopped = false;
            isProcessStart = false;
        }
    }


    public void MassgeStart()
    {
        //smoothLookat(new Vector3(0,0,0), 0.2f);
        if (StartTime > 0) StartTime -= Time.deltaTime;
        if (StartTime <= 0 && !isProcessStart)
        {
            FaceTowardCounter(180, 0.2f);
            AH.anim.Play("Massage");
            isProcessStart = true;
        }
    }
    void FaceTowardCounter(float direction, float time)
    {
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }

    void smoothLookat(Vector3 target, float rotationSpeed)
    {
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("MP") && MP.agent.velocity.magnitude < 0.1f)
        {
            BED = other.GetComponentInParent<Bed>();
        }
        if (other.CompareTag("MP") && MP.agent.velocity.magnitude > 0)
        {
            BED = null;
        }
    }
}
