using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Massger : MonoBehaviour
{
    private movePlayer MP;
    public CharacterController controller;
    public FloatingJoystick FJ;
    public animationHandler AH;
    public Bed BED;
    public GameObject Inventory;
    public float StartTimer = 0.15f;
    float StartTime = 0;



    public bool isProcessStart;
    public Vector3 position;
    private void Awake()
    {
        MP = GetComponent<movePlayer>();
        StartTime = StartTimer;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        MassageProcess();
    }

    public void MassageProcess()
    {
        if(!isProcessStart && BED && BED.isPlayerNear && BED.isCustomerNear && !BED.waitTimer.isWaitTimerComplete)
        {
            controller.enabled = false;
            Inventory.SetActive(false);
            FJ.enabled=false;
            transform.position = position; 
            MassgeStart();           
        }
        if (isProcessStart && BED.waitTimer.isWaitTimerComplete)
        {
            controller.enabled = true;
            FJ.enabled = true;
            Inventory.SetActive(true);
            AH.anim.SetTrigger("ME");
            StartTime = StartTimer;
            isProcessStart = false;
        }
    }

    
    public void MassgeStart()
    {
        //smoothLookat(new Vector3(0,0,0), 0.2f);
        if (StartTime > 0) StartTime -= Time.deltaTime;
        if (StartTime <= 0 && !isProcessStart)
        {
            FaceTowardCounter(180, 0.2f);            
            AH.anim.Play("Massage");
            isProcessStart = true;
        }
    }
    void FaceTowardCounter(float direction, float time)
    {
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }

    void smoothLookat(Vector3 target, float rotationSpeed)
    {
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("MP") && MP.direction.magnitude < 0.1f)
        {
            position = other.transform.position;
             BED = other.GetComponentInParent<Bed>();
        }
        if(other.CompareTag("MP") && MP.direction.magnitude > 0)
        {
            BED = null;
        }
    }

}
