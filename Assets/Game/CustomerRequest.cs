using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum TypeRequest{
    Towle,
    Massage,
    Invalid
}
public enum AnimType
{
    Idle,
    In,
    Exit
}
public class CustomerRequest : MonoBehaviour
{
    public List<Sprite>RI = new List<Sprite>(); 
    public Image RequestedImage;
    public Animator Anim;
    private void Start()
    {
        RequestItem(TypeRequest.Invalid);
        
    }
    private void Update()
    {
        transform.forward = -Camera.main.transform.forward;
    }

    public void RequestItem(TypeRequest TR)
    {
        Animation(AnimType.In);
        switch (TR)
        {
            case TypeRequest.Towle:
                RequestedImage.sprite = RI[0];
                break;
            case TypeRequest.Massage:
                RequestedImage.sprite = RI[1];
                break;
            case TypeRequest.Invalid:
                RequestedImage.sprite = null;
                break;
        }
    }

    public void Animation(AnimType AT)
    {
        switch (AT)
        {
            case AnimType.Idle:
                Anim.Play("Idle");
                break;
            case AnimType.In:
                Anim.Play("In");
                break;
            case AnimType.Exit:
                Anim.Play("Exit");
                break;
        }
    }
}
