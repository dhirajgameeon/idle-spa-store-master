using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sofa : MonoBehaviour
{
    public CustomerSpawner CS;
    private void Update()
    {
        checkSeat();
    }
    void checkSeat()
    {
        foreach (Transform item in transform)
        {
            SofaSeat seat = item.GetComponent<SofaSeat>();
            if (seat)
            {
                
                if (!seat.isBooked && !CS.SofaSeatList.Contains(seat))
                {
                    CS.SofaSeatList.Add(seat);
                }
                if (seat.isBooked && CS.SofaSeatList.Contains(seat))
                {
                    CS.SofaSeatList.Remove(seat);
                }
            }
        }
    }
}
