using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowleTablePosition : MonoBehaviour
{
    public StaffAIManager SAM;
    public bool isBooked;

    private void Awake()
    {
        SAM = GetComponentInParent<ShopController>().SAM;
    }

    private void Update()
    {
        BookingCheck();
    }
    public void BookingCheck()
    {
        if (SAM)
        {
            if (isBooked && SAM.TTP.Contains(this))
            {
                SAM.TTP.Remove(this);
            }
            if (!isBooked && !SAM.TTP.Contains(this))
            {
                SAM.TTP.Add(this);
            }
        }
    }
}
