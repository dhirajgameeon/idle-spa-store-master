using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangingSlote : MonoBehaviour
{
    public CustomerSpawner CS;
    public bool isBooked = false;

    private void Update()
    {
        checkSLote();
    }
    void checkSLote()
    {
        if (!isBooked && !CS.changingSlotes.Contains(this))
        {
            CS.changingSlotes.Add(this);
        }
        if (isBooked && CS.changingSlotes.Contains(this))
        {
            CS.changingSlotes.Remove(this);
        }
    }
}
