using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HireEmployee : MonoBehaviour
{
    public bool isPlayerNear = false;

    public StaffAIManager SAM;
    public GameObject HireUI;
    public Transform PO;
    public bool isClosed = false;
    private void Update()
    {
        FindGameObject();
    }

    void FindGameObject()
    {
        if (isPlayerNear)
        {
            HireUI = SAM.HireUI;
            if (HireUI && !isClosed)
            {
                if (!HireUI.activeSelf)
                {
                    EvenManager.TriggerSFXOneShotPlayEvent(AudioID.MenuOpen);
                    HireUI.SetActive(true);
                }
               
                HireUI.GetComponent<HireUI>().hireEmployee = SAM;
            }
        }
        if(!isPlayerNear && HireUI)
        {
            HireUI.GetComponent<Animator>().Play("Exit");
            HireUI.GetComponent<HireUI>().hireEmployee = null;
            isClosed = true;
            HireUI = null;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            if (other.GetComponent<movePlayer>().direction.magnitude < 0.1f) isPlayerNear = true; else { isPlayerNear = false; }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        //other.tag
        if (isPlayerNear)
        { 
            isPlayerNear = false; 
        }
            isClosed = false; 
    }


}
