using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class customerSpeechBubble : MonoBehaviour
{
    public SpriteRenderer UI;
    public SpriteRenderer Emoji;

    public Transform UIHolder;
    public Transform EmojiHolder;

    public Animator EmojiHolderAnimation;

    private controlCustomer customer;
    private moveCustomer move;
    private GameManager manager;

    // Start is called before the first frame update
    void Start()
    {
        customer = GetComponentInParent<controlCustomer>();
        move = GetComponentInParent<moveCustomer>();
        move.CSB = GetComponent<customerSpeechBubble>();    
        
    }

    // Update is called once per frame
    void Update()
    {
        Holder();
        ItemUIViewer();
    }

    public void ItemUIViewer()
    {
        try
        {
            UI.sprite = GameManager.instance.CustomerUI[customer.NeedItemID];
            
        }
        catch
        {
            print("<color=red> Customer controller is missing !!</color>");
        }
    }

    void Holder()
    {
        UIHolder.forward = -Camera.main.transform.forward;
        EmojiHolder.forward = -Camera.main.transform.forward;        
    }
    public void EnableUI()
    {
        if (customer.isItemTookFromPlayer) UIHolder.gameObject.SetActive(false);
        if (!customer.isItemTookFromPlayer) UIHolder.gameObject.SetActive(true);
    }
    public void EnableEmoji(bool isActive)
    {
        EmojiHolder.gameObject.SetActive(isActive);
        Emoji.sprite = GameManager.instance.customEmoji[Random.Range(0, GameManager.instance.customEmoji.Count - 1)];
    }
    public void EmojiOut()
    {
        EmojiHolderAnimation.Play("out");
        StartCoroutine(emojiOut(1));
    }

   IEnumerator emojiOut(float t)
    {
        yield return new WaitForSeconds(t);
        EmojiHolder.gameObject.SetActive(false);
    }
}
