using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlSpwanObject : MonoBehaviour
{
    public List<GameObject> itemCollection = new List<GameObject>();
    public ParticleSystem SparkEffect;
    private void OnEnable()
    {
        foreach (GameObject item in itemCollection)
        {
            if(item.activeSelf)item.SetActive(false);

        }
    }

    public void SetActiveObject(int num, bool state = false)
    {
        itemCollection[num].SetActive(state);
        if (state) SparkEffect.Play();
    }
}
