using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlCustomerChangeColor : MonoBehaviour
{
    public List<Color> colorList = new List<Color>();
    public SkinnedMeshRenderer Renderer;

    private void OnEnable()
    {
        changeColor();
    }
    void changeColor()
    {
        Renderer.materials[0].color = colorList[Random.Range(0, colorList.Count)];
    }
}
