using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlItem : MonoBehaviour
{
    public Shop shop;

    [Space(5)]
    public int ItemID = 0;
    public int ItemHeight = 1;
    //public float ItemHeightStack = 0.25f;


    public Vector3 EndPosition;
    public Collider other;
    public float moveSpeed = 5;
    public float rotationSpeed = 20;
    [Space(5)]
    public float Rotation = 0;

    public bool isMove = false;
    public bool isStacked = false;
    public bool isOccupied = false;
    public bool isPlayerCollected = false;

    private float normalSpeed = 0;
    private void Start()
    {
        //Rotation = Random.Range(-20, 20);
        normalSpeed = moveSpeed;
    }

    private void Update()
    {
        MoveObject();        
    }

    public void MoveObject()
    {
        if (isMove)
        {
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, EndPosition, moveSpeed * Time.deltaTime);
            //transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0,Rotation,0), rotationSpeed * Time.deltaTime);
        }
        if(isMove && Vector3.Distance(transform.localPosition, EndPosition) < 0.3f)
        {
            transform.localPosition = EndPosition;
            transform.localRotation = Quaternion.Euler(0, 0, 0);
            //transform.localRotation = Quaternion.Euler(0, Rotation, 0);
            GetComponent<Collider>().isTrigger = false;
            //if(ParantSet)transform.parent = ParantSet;
            if(other)other.GetComponent<controlTowleTable>().Towle.Add(transform);
            
            isMove = false;
        }
    }

    
    public void ResetTowle()
    {
        moveSpeed = normalSpeed;
        transform.parent = shop.inventory;
        transform.position = shop.inventory.position;
        gameObject.SetActive(false);
    }

    void resetObject(Collider other)
    {
        moveSpeed = normalSpeed;
        transform.parent = shop.inventory;
        transform.position = shop.inventory.position;
        gameObject.SetActive(false);
        other.GetComponent<Collider>().enabled = false;
        /* if(!shop.ShopItem.Contains(gameObject))
             shop.ShopItem.Add(gameObject);*/
    }

    void dustbin()
    {
        moveSpeed = normalSpeed;
        transform.parent = shop.inventory;
        transform.position = shop.inventory.position;
        gameObject.SetActive(false);
    }
    public void ShakeObject(GameObject dustbin)
    {
        if (!dustbin.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("Dustbin Shake"))
        {
            dustbin.GetComponent<Animator>().Play("Dustbin Shake");
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.DustbinShake);
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("customer"))
        {
            resetObject(other);            
        }
        if (other.CompareTag("Dustbin"))
        {
            dustbin();
            ShakeObject(other.transform.GetChild(1).gameObject);
        }
        if (other.CompareTag("drop"))
        {
            Destroy(this.gameObject, 0.01f);
        }
    }
}
