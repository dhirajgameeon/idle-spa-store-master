using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlEmp : MonoBehaviour
{
    public int employeeID = 0;
    public int CartLimit;
    public float stackTime = 0.2f;
    public Vector3 startStackPosition;
    public Transform Inventory;
    public Shop shop;
    public bool isEmpOnCollector;

    public List<GameObject> Cart = new List<GameObject>();
    private float stackTimerTime = 0.2f;
    private moveEmp moveEmp;

    private void Start()
    {
        moveEmp = GetComponent<moveEmp>();
    }

    void ArrangeGameObject()
    {
        if (Cart.Count == 1)
        {
            Cart[0].GetComponent<controlItem>().EndPosition = startStackPosition;
            Cart[0].GetComponent<controlItem>().isPlayerCollected = true;
            Cart[0].GetComponent<controlItem>().isStacked = true;
            Cart[0].GetComponent<controlItem>().isMove = true;
            return;
        }
    }
    void stackObject()
    {
        if(isEmpOnCollector && CartLimit > 0)
        {
            if(stackTimerTime>0)stackTimerTime-=Time.deltaTime;
            if (stackTimerTime <= 0)
            {
                try
                {
                    if(shop && shop.ShopItem.Count > 0)
                    {
                        if (!Cart.Contains(shop.ShopItem[0]))
                        {
                            Cart.Add(shop.ShopItem[0]);
                            Cart[Cart.Count - 1].transform.parent = Inventory;
                            Cart[Cart.Count - 1].transform.rotation = Quaternion.identity;
                            Cart[Cart.Count - 1].SetActive(true);
                            shop.ShopItem.Remove(shop.ShopItem[0]);
                            stackTimerTime = stackTime;
                            CartLimit -= 1;
                            return;
                        }
                    }
                    else
                    {
                        print(transform.name + "<color=red> Shop reference is missing !</color>");
                    }
                }
                catch
                {
                    print(transform.name +  "<color=red> Unexpected error !!</color>");
                }
            }
        }
    }
    void giveItemToCustomer(Collider other, int ID)
    {
        if (Cart.Count > 0)
        {
            for (int i = 0; i < Cart.Count;)
            {
                if (Cart[i].GetComponent<controlItem>().ItemID != ID) i++;
                if (Cart[i].GetComponent<controlItem>().ItemID == ID)
                {
                    Cart[i].transform.parent = null;
                    Cart[i].GetComponent<controlItem>().other = other;
                    Cart[i].GetComponent<controlItem>().EndPosition = other.GetComponent<controlTowleTable>().inventory.position;
                    Cart[i].GetComponent<controlItem>().isPlayerCollected = false;
                    Cart[i].GetComponent<controlItem>().isStacked = false;
                    Cart[i].GetComponent<controlItem>().isMove = true;
                    Cart.Remove(Cart[i]);
                    /*other.GetComponent<controlCustomer>().isItemTookFromPlayer = true;*/
                    other.GetComponent<controlTowleTable>().isTowlePresent = true;
                    CartLimit += 1;
                    moveEmp.ResetStuffAfterGiving();
                    break;
                }
            }
        }
    }

    float trashItemTimer = 0.01f;
    void throwInDustBin(Collider other)
    {
        if (Cart.Count > 0 && other.CompareTag("Dustbin") && moveEmp.agent.velocity.magnitude < 0.1f)
        {
            if (trashItemTimer > 0) trashItemTimer -= Time.deltaTime;
            if (trashItemTimer <= 0)
            {
                Cart[0].transform.parent = null;
                Cart[0].GetComponent<controlItem>().EndPosition = other.transform.position;
                Cart[0].GetComponent<controlItem>().isPlayerCollected = false;
                Cart[0].GetComponent<controlItem>().isStacked = false;
                Cart[0].GetComponent<controlItem>().isMove = true;
                Cart.Remove(Cart[0]);
                CartLimit += 1;
                trashItemTimer = 0.01f;
            }
        }
    }
    void onTriggerCustomer(Collider other)
    {
        if (other.CompareTag("TowleTable"))
        {
            try
            {
                //if (!other.GetComponent<controlCustomer>().isItemTookFromPlayer) giveItemToCustomer(other, other.GetComponent<controlCustomer>().NeedItemID);
                if (!other.GetComponent<controlTowleTable>().isTowlePresent) giveItemToCustomer(other, other.GetComponent<controlTowleTable>().NeedItemID);
            }
            catch
            {
                print("<color=red>Error</color>");
            }

        }
    }
    void checkForSurroundings(Collider other, bool onExit = false)
    {
        if (other.CompareTag("collect") && moveEmp.agent.velocity.magnitude < 0.1f)
        {
            isEmpOnCollector = true;
            shop = other.GetComponentInParent<Shop>();
        }
        if (onExit)
        {
            if (isEmpOnCollector)
            {
                if (shop) shop = null;
                isEmpOnCollector = false;
            }
        }
    }
    private void OnTriggerStay(Collider other)
    {
        checkForSurroundings(other);
        stackObject();
        ArrangeGameObject();
        onTriggerCustomer(other);
        throwInDustBin(other);
    }
    private void OnTriggerExit(Collider other)
    {
        checkForSurroundings(other, true);
    }
}
