using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class controlEmpUnlocker : MonoBehaviour
{
    [Header("Self Objects")]
    public Collider col;
    public GameObject UI;

    [Header("Border Expander")]
    [Space(10)]
    public Transform border;
    public float Speed = 5;
    public float maxSize = 1.5f;


    [Space(10)]
    public GameObject Emp;
    public GameObject LOCKED;
    public ParticleSystem unlockEffect;
    public TMP_Text amountText;
    public int ID = 8;
    public float amount;
    public float reduceAmountSpeed = 150;
    public bool isUnlocked=false;
    public bool isPlayerNear = false;

    public AudioSource cashSound;
    private void Start()
    {
        unlocked();
        if (LevelManager.instance.Level < 3 && !isUnlocked) Self(false);
        size = 1.5f;
        cashSound.enabled = false;
    }

    void Self(bool isOn)
    {
        col.enabled = isOn;
        UI.SetActive(isOn);
    }
    private void Update()
    {
        moneyManagement();
        borderExpander();
        if (LevelManager.instance.Level >= 3 && !isUnlocked) Self(true);
    }

    float size = 1.5f;
    void borderExpander()
    {
        if (isPlayerNear && size < maxSize) size += Speed * Time.deltaTime;
        if (!isPlayerNear && size >= 1.5f) size -= Speed * Time.deltaTime;
        border.localScale = new Vector3(size, size, 1);
    }

    void unlocked()
    {
        if (isUnlocked || amount <= 0)
        {
            Emp.SetActive(true);
            LOCKED.SetActive(false);
            GameManager.instance.unlockedEmployee++;
        }
    }

    void moneyManagement()
    {
        amountText.text = amount.ToString("0");
        if (!isUnlocked && amount < 1)
        {
            //Destroy(Instantiate(ParticalEffectForUnlocking, transform.position, Quaternion.identity), 5);
            Emp.SetActive(true);
            LOCKED.SetActive(false);
            unlockEffect.Play();
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.StaffSpawn);
            GameManager.instance.unlockedEmployee++;
            //GameManager.instance.instance.UnlockedShopCount++;
            //AudioManager.source.PlayOneShot(AudioManager.areaUnlock);
            isUnlocked = true;
        }

        if (!isUnlocked && isPlayerNear)
            moneyReducer();

    }
/*    float x = 0.15f;*/
    void moneyReducer()
    {
        if (amount > 0 && GameManager.instance.maxCash > 0)
        {
            amount -= reduceAmountSpeed * Time.deltaTime;
            GameManager.instance.maxCash -= reduceAmountSpeed * Time.deltaTime;
            cashSound.enabled = true;
            /* if (amount > 5)
             {
                 if (x > 0)
                     x -= Time.deltaTime;
                 if (x <= 0)
                 {
                     //moneyDeductationUI();
                     x = 0.15f;
                 }
             }*/

        }
        if (!isPlayerNear || (amount > 0 && GameManager.instance.maxCash < 1))
        {
            cashSound.enabled = false;
        }
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            if (other.GetComponent<movePlayer>().direction.magnitude < 0.1f)
            {
                isPlayerNear = true;
            }
            if (other.GetComponent<movePlayer>().direction.magnitude > 0)
            {
                isPlayerNear = false;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isPlayerNear) isPlayerNear = false;
        cashSound.enabled = false;
    }
}
