using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlTutorial : MonoBehaviour
{
    public bool isTutorialComplete=false;
    public GameObject arrow;

    private controlCustomer customer;

    private void Awake()
    {
        customer = GetComponent<controlCustomer>();
    }
    private void Start()
    {
        arrow.SetActive(false);
    }

    private void Update()
    {
        activeArrow();
    }
    void activeArrow()
    {
        if (LevelManager.instance.Level < 1)
        {
            if (customer.isItemTookFromPlayer) arrow.SetActive(false);
        }
    }
}
