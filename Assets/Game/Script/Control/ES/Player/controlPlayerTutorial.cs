using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPlayerTutorial : MonoBehaviour
{
    public Transform[] Target;
    public GameObject[] StartGame;
    public GameObject[] arrow;

    [Space(15)]   
    public GameObject LevelUp;
    public Transform PointingArrow;
    public Transform customer;
    public ParticleSystem confetti;

    [Space(15)]
    public Shop shop;
    public LevelManager lm;
    public GameManager gameManager;
    public controlStack controlStack;
    public Unlocker unlocker;

    [Space(15)]
    public Animator cam;
    
    [Space(15)]
    public bool isTutorialCompleted = false;
    public bool isPlayerOnDustbin = false;
    public bool cameraPanningForStaff = false;
    [HideInInspector]public int customerServed = 0;

    private void Awake()
    {
        if (lm.Level > 1)
        {
            atLevel_3();
        }
    }
    private void Update()
    {
        getCustomerLocation();
        if(!isTutorialCompleted) checkData();

        cameraPanning();
    }
    void atLevel_1()
    {
        

        if (controlStack.Cart.Count <= 0 && customer)
        {
            PointingArrow.gameObject.SetActive(true);
            lookAt(Target[0]);
            arrow[0].SetActive(true);
        }
        
        if(controlStack.Cart.Count > 0 && customerServed < 2)
        {
            if (customer)
            {
                arrow[0].SetActive(false);
                PointingArrow.gameObject.SetActive(true);
                customer.GetChild(4).gameObject.SetActive(true);
                lookAt(customer);
            }                                   
        }
        if (customerServed >= 2)
        {
            LevelUp.SetActive(true);
            PointingArrow.gameObject.SetActive(false);
        }
    }
    void atLevel_2()
    {
        if (customerServed >= 2 && unlocker.isLocked)
        {
            StartGame[5].SetActive(true);
            lookAt(Target[1]);
            arrow[1].SetActive(true);
            PointingArrow.gameObject.SetActive(true);
        }
        if (!unlocker.isLocked)
        {
            lookAt(Target[2]);
            arrow[1].SetActive(false);
            arrow[2].SetActive(true);
            
        }
        if (/*Vector3.Distance(transform.position, Target[2].position) < 2f*/ isPlayerOnDustbin && !unlocker.isLocked && !isTutorialCompleted)
        {
            PointingArrow.gameObject.SetActive(false);
            arrow[2].SetActive(false);
            confetti.Play();
            atLevel_3();
            lm.isSpawnActive = true;
            isTutorialCompleted = true;
        }
    }
    void atLevel_3()
    {
        StartGame[0].SetActive(false);
        StartGame[1].SetActive(true);
        StartGame[2].SetActive(true);
        StartGame[3].SetActive(true);
        StartGame[4].SetActive(true);
        StartGame[5].SetActive(true);

    }
    void getCustomerLocation()
    {
        if (!customer && shop.CustomerOnStore.Count > 0)
        {
            if (shop.CustomerOnStore[0].isStartTrade && !shop.CustomerOnStore[0].control.isItemTookFromPlayer)
            {
                customer = shop.CustomerOnStore[0].transform;                
            }
        }
        if (customer && customer.GetComponent<controlCustomer>().isItemTookFromPlayer)
        {            
            customer = null;
        }
    }
    void checkData()
    {
        if(lm.Level < 3)
        {
            if(lm.Level == 0)
            {
                atLevel_1();
            }
            if(lm.Level == 1)
            {
                atLevel_2();
                LevelUp.SetActive(false);
            }
        }
    }
    void lookAt(Transform Target)
    {
        PointingArrow.LookAt(new Vector3(Target.position.x, transform.position.y, Target.position.z));
    }
    void cameraPanning()
    {
        if(lm.Level >2 && !cameraPanningForStaff)
        {
            cam.Play("staff");
            cameraPanningForStaff = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Dustbin"))
        {
            isPlayerOnDustbin = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        isPlayerOnDustbin = false;
    }
}
