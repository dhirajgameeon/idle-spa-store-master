using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlStack : MonoBehaviour
{

    public int stackLimit;
    public int CartLimit;
    public float stackTime = 0.2f;
    public float YPosSpeedForCameraFollower=5;
    public Vector3 startPosition;
    public CharacterController controller;
    public Transform stackTransform;
    public Transform cameraFollower;
    public Shop Shop;

    public bool isPlayerOnCollector;
    public bool isPlayerOnDropper;

    public List<GameObject> Cart = new List<GameObject>();
    [HideInInspector] public movePlayer movePlayer;
    private controlPlayer cplayer;
    private float stackTimerTime = 0.2f;

    void Start()
    {
        cplayer = GetComponent<controlPlayer>();
        movePlayer = GetComponent<movePlayer>();
    }

    void Update()
    {
        if (Cart.Count > 0) arrange();
        cameraFollowerManager(cameraFollower);
    }

    float cameraFollowerYPos = 0;
    void cameraFollowerManager(Transform t)
    {
        if (cameraFollowerYPos < (Cart.Count / 4))
        {
            cameraFollowerYPos += YPosSpeedForCameraFollower * Time.deltaTime;
            if (cameraFollowerYPos >= (Cart.Count / 4))
                cameraFollowerYPos = (Cart.Count / 4);
        }
        if (cameraFollowerYPos > (Cart.Count / 4))
        {
            cameraFollowerYPos -= YPosSpeedForCameraFollower * Time.deltaTime;
            if (cameraFollowerYPos <= (Cart.Count / 4))
                cameraFollowerYPos = (Cart.Count / 4);
        }


        t.position = new Vector3(t.position.x, cameraFollowerYPos, t.position.z);
        controller.radius = 0.5f;


    }
    void arrange()
    {        
        for (int i = 1; i < Cart.Count - 1; i++)
        {

            if (Cart[i-1].GetComponent<controlItem>().ItemHeight == 1)
            {
                Cart[i].GetComponent<controlItem>().EndPosition =
                    new Vector3(Cart[i - 1].GetComponent<controlItem>().EndPosition.x,
                    Cart[i - 1].GetComponent<controlItem>().EndPosition.y + /*0.28f*/0.142f,
                    Cart[i - 1].GetComponent<controlItem>().EndPosition.z);
                Cart[i].transform.GetComponent<controlItem>().isMove = true;

            }
            if (Cart[i - 1].GetComponent<controlItem>().ItemHeight == 2)
            {
                Cart[i].GetComponent<controlItem>().EndPosition =
                    new Vector3(Cart[i-1].GetComponent<controlItem>().EndPosition.x,
                    Cart[i-1].GetComponent<controlItem>().EndPosition.y + 0.52f,
                    Cart[i-1].GetComponent<controlItem>().EndPosition.z);
                Cart[i].transform.GetComponent<controlItem>().isMove = true;

            }
            if (Cart[i-1].GetComponent<controlItem>().ItemHeight == 3)
            {
                Cart[i].GetComponent<controlItem>().EndPosition =
                    new Vector3(Cart[i-1].GetComponent<controlItem>().EndPosition.x,
                    Cart[i-1].GetComponent<controlItem>().EndPosition.y + 0.76f,
                    Cart[i-1].GetComponent<controlItem>().EndPosition.z);
                Cart[i].transform.GetComponent<controlItem>().isMove = true;
                
            }
        }
        Cart[0].GetComponent<controlItem>().EndPosition = startPosition;
        Cart[0].GetComponent<controlItem>().isMove = true;
        
    }
    void ArrangeGameObject()
    {
        if (Cart.Count == 1)
        {
            Cart[0].GetComponent<controlItem>().EndPosition = startPosition;
            Cart[0].GetComponent<controlItem>().isPlayerCollected = true;
            Cart[0].GetComponent<controlItem>().isStacked = true;            
            Cart[0].GetComponent<controlItem>().isMove = true;
            return;
        }

        if(Cart.Count > 1)
        {
            if (Cart[Cart.Count-2].GetComponent<controlItem>().ItemHeight == 1)
            {
                Cart[Cart.Count - 1].GetComponent<controlItem>().EndPosition =
                    new Vector3(Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.x,
                    Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.y + /*0.28f*/0.142f,
                    Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.z);

                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isPlayerCollected = true;
                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isStacked = true;                
                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isMove = true;
            }
            if (Cart[Cart.Count - 2].GetComponent<controlItem>().ItemHeight == 2)
            {
                Cart[Cart.Count - 1].GetComponent<controlItem>().EndPosition =
                    new Vector3(Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.x,
                    Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.y + 0.52f,
                    Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.z);

                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isPlayerCollected = true;
                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isStacked = true;                
                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isMove = true;
            }
            if (Cart[Cart.Count - 2].GetComponent<controlItem>().ItemHeight == 3)
            {
                Cart[Cart.Count - 1].GetComponent<controlItem>().EndPosition =
                    new Vector3(Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.x,
                    Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.y + 0.76f,
                    Cart[Cart.Count - 2].GetComponent<controlItem>().EndPosition.z);

                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isPlayerCollected = true;
                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isStacked = true;                
                Cart[Cart.Count - 1].transform.GetComponent<controlItem>().isMove = true;
            }
        }
        
    }
    void stackObject()
    {
        if (isPlayerOnCollector && stackLimit > 0)
        {
            if (stackTimerTime > 0) stackTimerTime -= Time.deltaTime;
            if (stackTimerTime <= 0)
            {
                try{
                    if(Shop && Shop.ShopItem.Count > 0)
                    {
                        Shop s = Shop;
                        if (!Cart.Contains(s.ShopItem[0]))
                        {
                            Cart.Add(s.ShopItem[0]);
                            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PickUp);
                            Cart[Cart.Count - 1].transform.parent = stackTransform;
                            Cart[Cart.Count - 1].transform.rotation = Quaternion.identity;
                            Cart[Cart.Count - 1].SetActive(true);
                            s.ShopItem.Remove(s.ShopItem[0]);
                            stackTimerTime = stackTime;
                            stackLimit -= 1;
                            return;
                        }
                    }
                    else { print(transform.name + "<color=red> Shop reference is missing  !</color>"); }
                }
                catch{ print(transform.name +  "<color=red> Unexpected error !!</color>"); }
            }
        }
    }
    public void stackCash(GameObject cash)
    {
        if (stackLimit > 0)
        {
            try
            {
                ArrangeGameObject();
                if (!Cart.Contains(cash))
                {
                    Cart.Add(cash);
                    //EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PickUp);
                    Cart[Cart.Count - 1].transform.parent = stackTransform;
                    Cart[Cart.Count - 1].transform.rotation = Quaternion.identity;
                    Cart[Cart.Count - 1].SetActive(true);
                    stackTimerTime = stackTime;
                    stackLimit -= 1;
                    return;
                }

                else { print(transform.name + "<color=red> Shop reference is missing  !</color>"); }
            }
            catch { print(transform.name + "<color=red> Unexpected error !!</color>"); }
        }
    }

    void GiveItemToCustomer(Collider other, int needItem)
    {
        if (Cart.Count > 0)
        {
            for (int i = 0; i < Cart.Count;)
            {
                /*if (i >= Cart.Count - 1 && Cart[i].GetComponent<controlItem>().ItemID != needItem)
                    return;*/
                if (Cart[i].GetComponent<controlItem>().ItemID != needItem) i++;
                if(Cart[i].GetComponent<controlItem>().ItemID == needItem)
                {
                    EvenManager.TriggerSFXOneShotPlayEvent(AudioID.GiveItem);
                    Cart[i].transform.parent = null;
                    Cart[i].GetComponent<controlItem>().other = other;
                    Cart[i].GetComponent<controlItem>().EndPosition = other.GetComponent<controlTowleTable>().inventory.position;/*new Vector3(other.transform.position.x, other.transform.position.y + 1, other.transform.position.z);*/
                    Cart[i].GetComponent<controlItem>().isPlayerCollected = false;
                    Cart[i].GetComponent<controlItem>().isStacked = false;
                    Cart[i].transform.GetComponent<controlItem>().moveSpeed = Cart[i].transform.GetComponent<controlItem>().moveSpeed / 3;
                    Cart[i].GetComponent<controlItem>().isMove = true;
                    Cart.Remove(Cart[i]);
                    other.GetComponent<controlTowleTable>().isTowlePresent = true;                    
                    stackLimit += 1;
                    break;
                }
            }
        }
       
    }

    float x = 0.35f;
    public void dropCash(Collider other, int need)
    {
        if (Cart.Count > 0)
        {
            if (x > 0) x -= Time.deltaTime;
            if (x <= 0)
            {
                for (int i = 0; i < Cart.Count;)
                {
                    /*if (i >= Cart.Count - 1 && Cart[i].GetComponent<controlItem>().ItemID != needItem)
                        return;*/
                    if (Cart[i].GetComponent<controlItem>().ItemID != need) i++;
                    if (Cart[i].GetComponent<controlItem>().ItemID == need)
                    {
                        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.GiveItem);
                        Cart[i].transform.parent = null;
                        Cart[i].GetComponent<controlItem>().EndPosition = new Vector3(other.transform.position.x, other.transform.position.y - 1, other.transform.position.z);
                        Cart[i].GetComponent<controlItem>().isPlayerCollected = false;
                        Cart[i].GetComponent<controlItem>().isStacked = false;
                        Cart[i].transform.GetComponent<controlItem>().moveSpeed = Cart[i].transform.GetComponent<controlItem>().moveSpeed / 1.5f;
                        Cart[i].GetComponent<controlItem>().isMove = true;
                        Cart.Remove(Cart[i]);
                        stackLimit += 1;
                        x = 0.35f;
                        break;
                    }
                }
            }

        }
    }
    void onTriggerCustomer(Collider other)
    {
        if (other.CompareTag("TowleTable"))
        {
            try
            {
                //if (!other.GetComponent<controlCustomer>().isItemTookFromPlayer) GiveItemToCustomer(other, other.GetComponent<controlCustomer>().NeedItemID);
                if (!other.GetComponent<controlTowleTable>().isTowlePresent) GiveItemToCustomer(other, other.GetComponent<controlTowleTable>().NeedItemID);
            }
            catch
            {
                print("<color=red>Error</color>");
            }
           
        }
        if (other.CompareTag("drop") && movePlayer.direction.magnitude < 0.1f)
        {
            try
            {
                if (other.GetComponentInParent<Unlocker>().isLocked) dropCash(other, other.GetComponentInParent<Unlocker>().ID);
                if (!other.GetComponent<controlEmpUnlocker>().isUnlocked) dropCash(other, other.GetComponent<controlEmpUnlocker>().ID);
            }
            catch
            {
                print("<color=red>error in cash drop</color>");
            }
        }
    }
    float trashItemTimer = 0.1f;
    void throwInDustBin(Collider other)
    {
        if (Cart.Count > 0 && other.CompareTag("Dustbin") && movePlayer.direction.magnitude < 0.1f)
        {
            if (trashItemTimer > 0) trashItemTimer -= Time.deltaTime;
            if (trashItemTimer <= 0)
            {
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PickUp);
                Cart[0].transform.parent = null;
                Cart[0].GetComponent<controlItem>().EndPosition = other.transform.position;
                Cart[0].GetComponent<controlItem>().isPlayerCollected = false;
                Cart[0].GetComponent<controlItem>().isStacked = false;
                Cart[0].GetComponent<controlItem>().isMove = true;
                Cart[0].GetComponent<controlItem>().moveSpeed = Cart[0].GetComponent<controlItem>().moveSpeed / 3;
                Cart.Remove(Cart[0]);
                stackLimit += 1;
                trashItemTimer = stackTime / 3f;
            }
        }       
    }
    private void OnTriggerStay(Collider other)
    {
        checkForSurroundings(other);
        stackObject();
        ArrangeGameObject();
        onTriggerCustomer(other);
        throwInDustBin(other);
    }

    private void OnTriggerExit(Collider other)
    {
        checkForSurroundings(other, true);
    }
    void checkForSurroundings(Collider other, bool onExit = false)
    {
        if (other.CompareTag("collect") && movePlayer.direction.magnitude < 0.1f)
        {
            isPlayerOnCollector = true;
            Shop = other.GetComponentInParent<Shop>();
        }
        if (other.CompareTag("drop") && movePlayer.direction.magnitude < 0.1f) isPlayerOnDropper = true;

        if (onExit && movePlayer.direction.magnitude > 0.1f)
        {
            if (isPlayerOnDropper) isPlayerOnDropper = false;
            if (isPlayerOnCollector)
            {
                if (Shop) Shop = null;
                isPlayerOnCollector = false;
            }
        }
    }
}
