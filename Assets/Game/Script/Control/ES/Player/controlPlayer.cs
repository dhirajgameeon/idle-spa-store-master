using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPlayer : MonoBehaviour
{
    public Statistic playerStatistic;
    public Transform maxCounter;
    public MeshRenderer renderer;
    public bool isDied = false;
    
    [HideInInspector]public animationHandler aPlayer;
    private controlStack stack;

    bool isSetActive = false;
    void Start()
    {
        aPlayer = GetComponent<animationHandler>();
        stack = GetComponent<controlStack>();
    }    
    void Update()
    {
        if (stack.Cart.Count > (stack.CartLimit - 1)) maxCounter.localPosition = new Vector3(stack.Cart[stack.Cart.Count - 1].transform.localPosition.x, stack.Cart[stack.Cart.Count - 1].transform.position.y + 1.5f, 0.8f);
        if (stack.Cart.Count >= stack.CartLimit && !isSetActive)
        {
            Invoke(nameof(setActive), 0.1f);            
            isSetActive = true;
            print("Active");
        }
        if(stack.Cart.Count < stack.CartLimit && isSetActive )
        {
            renderer.enabled = false;
            isSetActive = false;
        }
    }
    
    void setActive()
    {
        renderer.enabled = true;
    }
}
