using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlStandingPosition : MonoBehaviour
{

    private Unlocker unlocker;
    [HideInInspector]public Shop shop;
    private SectionManager sectionManager;    
    public bool isOccupied;
    
    public int ID=0;
    public float rotationFace = 90;
    /// <summary>
    /// Note : Decomment all lines when you create "Game Manager and Customer Spwan Script" and add required lists.
    /// </summary>

    void Start()
    {
        unlocker = GetComponentInParent<Unlocker>();
        shop = GetComponentInParent<Shop>();
        //sectionManager = unlocker.sectionManager;
        ID = shop.ShopID;
    }


    void Update()
    {
        standingPositionUpdate();
    }

    void standingPositionUpdate()
    {
        if (!unlocker.isLocked)
        {
            if (!isOccupied && !sectionManager.CSP.Contains(this.gameObject)) sectionManager.CSP.Add(this.gameObject);

            if (isOccupied && sectionManager.CSP.Contains(this.gameObject)) sectionManager.CSP.Remove(this.gameObject);
        }
    }

}
