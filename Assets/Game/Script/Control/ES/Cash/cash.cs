using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cash : MonoBehaviour
{
    public bool isPlay=false;
    public GameObject CashObject;
    public float force = 10;
    public float UISpeed = 1f;
    public int cashValue = 10;

    Rigidbody rb;
    Collider col;

    private Transform Player;
    private controlStack playerStack;
    private controlUIMoney CUM;
    Vector3 cashPos;
    private void Awake()
    {
        Player = FindObjectOfType<controlPlayer>().transform;
        rb = GetComponent<Rigidbody>();
        col = GetComponent<Collider>();
        playerStack = Player.GetComponent<controlStack>();
        CUM = FindObjectOfType<controlUIMoney>();
    }

    private void OnEnable()
    {
        transform.rotation = Random.rotation;
        rb.AddForce(transform.up * force, ForceMode.Impulse);
        rb.AddForce(transform.forward * (force / 2), ForceMode.Impulse);
        playAudio = Random.Range(0, 2);
    }

    int playAudio = 0;
    private void Update()
    {
        if(Player)cashPos = new Vector3(Player.position.x, Player.position.y + 2.5f, Player.position.z);
    }

    public void CashUIUpdate()
    {
        GameObject UI = CUM.CashUI[CUM.CashUI.Count - 1].gameObject;
        CUM.CashUI.Remove(UI.transform);
        UI.SetActive(true);
        UI.transform.position = Camera.main.WorldToScreenPoint(transform.position);
/*        RectTransform RUI = UI.GetComponent<RectTransform>();*/

        LeanTween.moveLocal(UI, CUM.cashPosition.localPosition, UISpeed).setOnComplete(() => {
           
            GameManager.instance.cashIconAnimation();
            GameManager.instance.maxCash += cashValue;
            UI.gameObject.SetActive(false);
            CUM.CashUI.Add(UI.transform);        
            GameManager.instance.resetIconSize();
        });
    }


    void onCollitionToPlayer(Collider other)
    {
        LeanTween.move(this.gameObject, cashPos, 0.2f).setOnComplete(() => {
            //spwanCash();
            CashUIUpdate();            
            Destroy(this.gameObject);            
        });
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.CashPick);
        
        //GameManager.instance.maxCash += cashValue;
    }

    void spwanCash()
    {
        if(playerStack.Cart.Count < 8 && GameManager.instance.isSpwanCash)
        {
            GameObject obj = Instantiate(CashObject, transform.position, Quaternion.identity);
            //playerStack.stackCash(obj);
        }        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ground"))
        {
            transform.rotation = Quaternion.Euler(0, 0, 0);
            rb.isKinematic = true;
            col.isTrigger = true;
        }
       
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Player = other.transform;            
            onCollitionToPlayer(other);            
            Destroy(col);
        }
    }
}
