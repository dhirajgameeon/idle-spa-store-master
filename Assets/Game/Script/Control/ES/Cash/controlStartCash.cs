using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlStartCash : MonoBehaviour
{
    public float ForceAmount = 50;
    public float LeanTweenDelayCall = 1.5f;
    public float moveTime = 0.5f;
    public int cashAmount = 3;
    public GameObject CashObject;

    public bool isPlay = false;
    public bool isSound = false;
    public bool isAnimationHappned = false;
    GameObject player;
    controlStack playerStack;
    GameManager gameManager;
    Rigidbody Rigidbody;     
    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
        gameManager = FindObjectOfType<GameManager>();
        playerStack = player.GetComponent<controlStack>();
        ForceAmount = Random.Range(7, 15);
    }
    
    public void setRotation()
    {
        Rigidbody.isKinematic = false;
        isAnimationHappned = true;
        Rigidbody.AddForce(new Vector3(Random.Range(-1f * ForceAmount, 1f * ForceAmount), ForceAmount, Random.Range(-1f * ForceAmount, 1f * ForceAmount)), ForceMode.VelocityChange);
        LeanTween.rotateLocal(gameObject, new Vector3(Random.Range(10, 180), Random.Range(10, 180), Random.Range(10, 180)), LeanTweenDelayCall);
        LeanTween.delayedCall(LeanTweenDelayCall, () => {
            GetComponent<Rigidbody>().isKinematic = true;
            moveTowardsPlayer();
        });
    }
    void moveTowardsPlayer()
    {
        LeanTween.rotateLocal(gameObject, new Vector3(Random.Range(10, 180), Random.Range(10, 180), Random.Range(10, 180)), moveTime / 1.2f);
        LeanTween.scale(gameObject, new Vector3(0.3f, 0.3f, 0.3f), moveTime);
        LeanTween.move(gameObject, player.transform.position+new Vector3(0,1.5f,0), moveTime).setOnComplete(() => {            
            gameObject.SetActive(false);
            if (isPlay) { 
                spwanCash();                
                gameManager.maxCash += cashAmount;
            }
            if (isSound)
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.CashPick);
        });
    }
    void spwanCash()
    {
        if (playerStack.Cart.Count < 8 && gameManager.isSpwanCash)
        {
            GameObject obj = Instantiate(CashObject, transform.position, Quaternion.identity);
            playerStack.stackCash(obj);
        }
    }
}
