using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class controlCashCollector : MonoBehaviour
{
    public List<Transform> cash = new List<Transform>();
    public GameObject VFX;
    public NavMeshObstacle obstacle;
    public bool isPlayerNear = false;

    private void Awake()
    {
        
        foreach (Transform item in transform)
        {
            if (!cash.Contains(item))
                cash.Add(item);
        }
    }

    public void expandCash()
    {
        if (isPlayerNear)
        {
            foreach(Transform item in cash)
            {
                if(!item.GetComponent<controlStartCash>().isAnimationHappned) item.GetComponent<controlStartCash>().setRotation();
            }
            
        }
    }
    public bool isTrue;
    private void OnTriggerStay(Collider other)
    {
        isTrue = cash.TrueForAll(cash => cash.gameObject.activeSelf == false);
        if (other.CompareTag("Player") && other.GetComponent<movePlayer>().direction.magnitude < 0.1f)
        {
            isPlayerNear = true;
            VFX.SetActive(false);
            expandCash();
        }
        if (other.CompareTag("Player") && other.GetComponent<movePlayer>().direction.magnitude > 0)
        {
            isPlayerNear = false;
        }
        if (other.CompareTag("Player"))
        {

           
            if (isTrue)
            {
                obstacle.enabled = false;
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (isPlayerNear) isPlayerNear = false;
    }
}
