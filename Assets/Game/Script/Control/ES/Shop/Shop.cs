using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
    public int ShopID = 0;
    public Transform inventory;
    public List<GameObject>ShopItem = new List<GameObject>();
    public List<moveCustomer> CustomerOnStore = new List<moveCustomer>();
    public StaffAIManager SAM;
    private Unlocker unlocker;

    private void Start()
    {
        unlocker = GetComponentInParent<Unlocker>();
       // if(!unlocker.sectionManager.ListOfShop.Contains(this)) unlocker.sectionManager.ListOfShop.Add(this);
    }

    private void Update()
    {
        InventoryManagement();
        clearShopItem();
    }

    void InventoryManagement()
    {
        foreach (Transform item in inventory)
        {
            if (!ShopItem.Contains(item.gameObject))
            {
                if (item.GetComponent<controlItem>().shop == null) item.GetComponent<controlItem>().shop = this;
                item.gameObject.SetActive(false);
                ShopItem.Add(item.gameObject);
            }
        }
    }
    void clearShopItem()
    {
        if(ShopItem.Count > 0)
        {
            for (int i = 0; i < ShopItem.Count-1; i++)
            {
                if (ShopItem[i] == null)
                {
                    ShopItem.Remove(ShopItem[i]);
                }
            }
        }
    }
}
