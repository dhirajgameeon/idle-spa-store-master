using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerAnimationManager : MonoBehaviour
{
    public CustomerController CC;
    public CustomerMove CM;
    public Transform AnimatorTargets;
    public Animator anim;

    // Start is called before the first frame update
    void Start()
    {
        CM = GetComponent<CustomerMove>();
        CC = GetComponent<CustomerController>();
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorChanger();
        if (anim) Animation();
    }

    void AnimatorChanger()
    {
        foreach (Transform item in AnimatorTargets)
        {
            if (item.gameObject.activeSelf && item.GetComponent<Animator>())
            {
                anim = item.GetComponent<Animator>();
            }
        }
    }
    void Animation()
    {
        anim.SetFloat("speed", CC.agent.velocity.magnitude);
    }
    public void triggerAnimation(string s)
    {
        anim.Play(s);
    }
}
