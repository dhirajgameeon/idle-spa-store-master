using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerSpawner : MonoBehaviour
{

    public List<Transform> Spawns = new List<Transform>();
    public List<GameObject> Customer = new List<GameObject>();
    public List<SofaSeat> SofaSeatList = new List<SofaSeat>();
    public List<ChangingSlote> changingSlotes = new List<ChangingSlote>();

    public Transform ExitPosition;
    public Transform CustomerList;
    public Transform Counter;
    public BedManager BedManager;
    public bool StoreOpen;
    public float spawnTime = 1;

    private void Update()
    {
        ListCustomerManager();
        Spawner();
    }

    /// Customer Spewan Process
    float spawner = 0;
    public void Spawner()
    {
        if (!StoreOpen && BedManager.beds.Count > 0) StoreOpen = true;
        if (StoreOpen)
        {
            if (changingSlotes.Count > 0 && SofaSeatList.Count > 0)
            {
                if (Spawns.Count > 0 && Customer.Count > 0)
                {
                    if (spawner > 0) spawner -= Time.deltaTime;
                    if (spawner <= 0)
                    {
                        GameObject customer = Customer[Random.Range(0, Customer.Count - 1)];
                        Customer.Remove(customer);
                        customer.transform.parent = null;
                        customer.transform.position = Spawns[Random.Range(0, Spawns.Count - 1)].position;
                        customer.SetActive(true);
                        charAssignment(customer);
                        spawner = spawnTime;
                        return;
                    }
                }
            }
        }
      
    }
    void charAssignment(GameObject customer)
    {
        customer.GetComponent<CustomerController>().Parant = CustomerList;
        customer.GetComponent<CustomerController>().Targets[0] = changingSlotes[0].transform;
        customer.GetComponent<CustomerController>().Targets[1] = SofaSeatList[0].transform;
        customer.GetComponent<CustomerController>().Targets[3] = Counter;
        customer.GetComponent<CustomerController>().Targets[customer.GetComponent<CustomerController>().Targets.Length - 1] = ExitPosition;
        SofaSeatList[0].isBooked = true;
        changingSlotes[0].isBooked = true;
        customer.GetComponent<CustomerController>().bedManager = BedManager;
    }
    /// </summary>
    
    public void ListCustomerManager()
    {
        foreach (Transform item in CustomerList)
        {
            if (item.gameObject.activeSelf) {
                item.gameObject.SetActive(false);
                Customer.Add(item.gameObject);
            }
        }

    }
}
