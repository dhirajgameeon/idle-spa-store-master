using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerControls : MonoBehaviour
{
    private CustomerController CC;
    private CustomerMove CM;
    private CustomerAnimationManager CAM;

    public float JumpAnimationTime = 1;

    public GameObject Character_1;
    public GameObject Character_2;
    public GameObject Character_3;
    public GameObject Cash;

    public Collider col;
    public ParticleSystem puff;
    public GameObject puffObj;

    public CustomerRequest CR;
    private Transform Player;
    public float CashSpwanCount;
    public bool isPuff;
    public float DistanceTemp;
    public float distanceFromPlayer;

    private void Awake()
    {
        CC = GetComponent<CustomerController>();
        CM = GetComponent<CustomerMove>();
        CAM = GetComponent<CustomerAnimationManager>();
        CR = GetComponentInChildren<CustomerRequest>();

        Player = GameObject.Find("Player").transform;
    }

    private void OnEnable()
    {
        puff.Stop();
    }
    private void Update()
    {

        if (Player) distanceFromPlayer = Vector3.Distance(transform.position, Player.position);



        if (!CM.CustomerProcessCompleted)
        {
            ChangingCloth(0.5f);
            GoinOnBed(1);
            SeatOnSofa(1.1f);
            exitChanging();
        }
        if (CM.CustomerProcessCompleted)
        {
            exitChangingCloth(0.5f);
            MoneyThrow(1.6f);
            ResetCustomerWhileReachingExitPoint(0.5f);
        }

       /* if (Input.GetKeyDown(KeyCode.P))
            puff.Play();*/

        if (Input.GetKeyDown(KeyCode.S))
            puff.Stop();

    }
    void ChangingCloth(float distance)
    {
        
        if (!CM.isClothChanged)
        {            
            if (CC.Targets[0] && Vector3.Distance(transform.position, CC.Targets[0].position) <= distance)
            {
                if (!CC.agent.isStopped)
                {
                    //jump to change cloth
                    print("Jump Session Started");
                    StartCoroutine(changeCloth());
                    CC.agent.isStopped = true;
                }
            }
        }
    }
    IEnumerator changeCloth(bool isexit = false)
    {
        yield return new WaitForSeconds(0.15f);
        //trigger jump animation
        CAM.triggerAnimation("Jump");
        yield return new WaitForSeconds(JumpAnimationTime);        
        print("Cloth Changed");
        if (!isexit)
        {
            switchChar(1);
            if(distanceFromPlayer <= 8.5f)EvenManager.TriggerSFXOneShotPlayEvent(AudioID.SwapCloth);
            CM.isClothChanged = true;
            CC.Targets[0].GetComponent<ChangingSlote>().isBooked = false;
        }
        if (isexit)
        {
            switchChar(0);
            if (distanceFromPlayer <= 8.5f) EvenManager.TriggerSFXOneShotPlayEvent(AudioID.SwapCloth);
            CM.ChangedClothOnExit = true;
            CC.Targets[0].GetComponent<ChangingSlote>().isBooked = false;
        }

        yield return new WaitForSeconds(0.15f);
        CC.agent.isStopped = false;
    }
    
    void SeatOnSofa(float distance)
    {
        if (!CM.isBedOpen)
        {            
            if (CC.Targets[1] && Vector3.Distance(transform.position, CC.Targets[1].position) <= distance)
            {
                if (!CC.agent.isStopped)
                {
                    StartCoroutine(OnSofa(0.5f));
                    CC.agent.isStopped = true;
                }
            }
        }
    }

    IEnumerator OnSofa(float t)
    {
        yield return new WaitForSeconds(0.5f);
        transform.rotation = Quaternion.Euler(0,90,0);
        CAM.triggerAnimation("SeatIn");
        //Sitting Animation
        CM.isSitting = true;
        yield return new WaitForSeconds(t);

        print("Seat");
    }

    IEnumerator SeatExit(float t)
    {

        yield return new WaitForSeconds(0.35f);
        transform.rotation = Quaternion.Euler(0, 0, 0);
        yield return new WaitForSeconds(t);
        if(CC.Targets[1])CC.Targets[1].GetComponent<SofaSeat>().isBooked = false;
        CC.agent.isStopped = false;
        if(CC.Targets[1]) CC.Targets[1] = null;
    }

    bool isDesp;
    float x = 0.15f;
    Coroutine JaccuziCortn, MassageCortn,ExitCortn;
    void GoinOnBed(float distance)
    {

        if(CC.Targets[2]) DistanceTemp = Vector3.Distance(transform.position, CC.Targets[2].position);

        if (CM.isBedOpen && CC.Targets[1] && CM.isSitting)
        {
            StartCoroutine(SeatExit(0.35f));
            CAM.anim.SetTrigger("EXITS");
            CM.isSitting = false;
        }
        if (CM.isBedOpen)
        {           
            if (CC.Targets[1] /*&& CC.Targets[1].GetComponent<SofaSeat>().isBooked*/)
            {
                CAM.AnimatorTargets.localRotation = Quaternion.Euler(0, 0, 0);
                CC.Targets[1].GetComponent<SofaSeat>().isBooked = false;
                CC.Targets[1] = null;
                CC.agent.isStopped = false;
            }
            if (CC.Targets[2] && Vector3.Distance(transform.position, CC.Targets[2].position) <= distance)
            {
                if (!CC.agent.isStopped && !CM.tookTawle)
                {
                    colliderClose(false);
/*                    puff.Stop();*/
                    if (CC.Targets[2].GetComponent<Bed>().BedID == 0) JaccuziCortn = StartCoroutine(JaccuziProcess(1.5f));
                    if (CC.Targets[2].GetComponent<Bed>().BedID == 1) MassageCortn =  StartCoroutine(MassageProcess(0.25f, CC.Targets[2].GetComponent<Bed>()));
                    //Debug.Log("--");
                    //puff.Play();
                    CC.agent.isStopped = true;
                }
                if (CC.agent.isStopped)
                {
                    if (x > 0) x -= Time.deltaTime;
                    if (x <= 0)
                    {
                        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0, -90, 0), 100 * Time.deltaTime);
                    }
                   
                    //smoothLookat(transform.forward, 10f);
                }
                if (CC.Targets[2].GetComponent<Bed>().waitTimer.isWaitTimerComplete)
                {
                    CC.Targets[2].GetComponent<Bed>().waitTimer.isWaitTimerActive = false;
                    if (!CM.tookTawle)
                    {
                        if (CC.Targets[2].GetComponent<Bed>().CTT.Towle.Count > 0)
                        {
                           ExitCortn = StartCoroutine(GoingOutOfJaccuzi(1));                            
                        }
                        if (CC.Targets[2].GetComponent<Bed>().CTT.Towle.Count <= 0)
                        {
                            //Requesting For towle
                            if (CC.Targets[2].GetComponent<Bed>().BedID == 0)
                            {
                                CR.RequestItem(TypeRequest.Towle);
                                CR.Animation(AnimType.In);
                                /*print("No Towle near by");*/
                            }
                            if (CC.Targets[2].GetComponent<Bed>().BedID == 1 && CC.Targets[2].GetComponent<Bed>().waitTimer.isWaitTimerComplete)
                            {
                                CR.RequestItem(TypeRequest.Towle);
                                CR.Animation(AnimType.In);
                                /*print("No Towle near by");*/
                            }
                        }
                    }
                }
            }
            if (CC.Targets[2] && !CC.Targets[2].GetComponent<Bed>().waitTimer.isWaitTimerComplete)
            {
                if (CC.Targets[2].GetComponent<Bed>().isCustomerNear && !CC.Targets[2].GetComponent<Bed>().isPlayerNear)
                {
                    //print(transform.name + "<color=green>: Trigger</color>");
                    CR.Animation(AnimType.In);
                    CR.RequestItem(TypeRequest.Massage);
                }
                if (!isDesp && CC.Targets[2].GetComponent<Bed>().isCustomerNear && CC.Targets[2].GetComponent<Bed>().isPlayerNear)
                {
                    CR.RequestItem(TypeRequest.Massage);
                    CR.Animation(AnimType.Exit);
                    isDesp = true;
                }
            }
        }
    }


    public void exitChanging()
    {

        if(CM.tookTawle && !CM.isOnBed)
        {
            if (MassageCortn != null) StopCoroutine(MassageCortn);
            if (JaccuziCortn != null) StopCoroutine(JaccuziCortn);
            if (ExitCortn != null) StopCoroutine(ExitCortn);
            colliderClose();
            if (!CC.Targets[0].GetComponent<ChangingSlote>().isBooked)
            {
                CC.Targets[0].GetComponent<ChangingSlote>().isBooked = true;
                CC.Targets[2].GetComponent<Bed>().waitTimer.isWaitTimerComplete = false;
                CC.Targets[2].GetComponent<Bed>().isBooked = false;
                CC.Targets[2] = null;
                x = 0.15f;
                CM.CustomerProcessCompleted = true;
                CC.agent.isStopped = false;
            }
        }        
    }
    void FaceTowardCounter(GameObject  obj ,float direction, float time)
    {
        LeanTween.rotate(obj, new Vector3(0, direction, 0), time);
    }

    void smoothLookat(Vector3 target, float rotationSpeed)
    {
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }


    IEnumerator JaccuziProcess(float t)
    {
        yield return new WaitForSeconds(0.15f);
        LeanTween.rotate(gameObject, new Vector3(0, -90, 0), 0.1f);
        if (!isPuff)
        {
            //Debug.LogError("ouff");
            //puff.Play();
            Destroy(Instantiate(puffObj, transform.position + new Vector3(0, 1, 0), Quaternion.identity), 2.5f);
            if (distanceFromPlayer <= 8.5f) EvenManager.TriggerSFXOneShotPlayEvent(AudioID.JacuzziEnter);
            isPuff = true;
        }
        CAM.triggerAnimation("CJ");
        switchChar(2);
        CAM.AnimatorTargets.position = CC.Targets[2].GetComponent<Bed>().bathPosition.position;        
        //FaceTowardCounter(CAM.AnimatorTargets.gameObject, 90, 0.2f);

        //trigger animation on going in jaccuzi
        yield return new WaitForSeconds(t);
        CC.Targets[2].GetComponent<Bed>().waitTimer.isWaitTimerActive = true;
    }

    IEnumerator MassageProcess(float t, Bed bed)
    {

        yield return new WaitForSeconds(0.15f);
        if (!isPuff)
        {
            //if (puff.isPlaying) 
            //puff.Play();
            Destroy(Instantiate(puffObj, transform.position + new Vector3(0, 1, 0), Quaternion.identity), 2.5f);
            if (distanceFromPlayer <= 8.5f) EvenManager.TriggerSFXOneShotPlayEvent(AudioID.MassageEnter);
            isPuff = true;
        }
        yield return new WaitForSeconds(0.01f);
        CAM.triggerAnimation("CJ");                
        switchChar(2);
        CAM.AnimatorTargets.position = bed.bathPosition.position;

        //FaceTowardCounter(CAM.AnimatorTargets.gameObject, 90, 0.2f);

        yield return new WaitForSeconds(t);
        bed.isCustomerNear = true;

/*        CC.Targets[2].GetComponent<Bed>().waitTimer.isWaitTimerActive = true;*/
    }
    IEnumerator GoingOutOfJaccuzi(float t)
    {
        puff.Stop();
        yield return new WaitForSeconds(0.15f);
        CAM.anim.SetTrigger("CJ");
        if (isPuff)
        {
            //puff.Play();
            Destroy(Instantiate(puffObj, transform.position + new Vector3(0, 1, 0), Quaternion.identity), 2.5f);
            CR.Animation(AnimType.Exit);
            if (distanceFromPlayer <= 8.5f) EvenManager.TriggerSFXOneShotPlayEvent(AudioID.MassageExit);
            isPuff = false;
        }
        yield return new WaitForSeconds(0.1f);
        switchChar(1,false);
        CAM.AnimatorTargets.localPosition = new Vector3(0, 0, 0);
        //FaceTowardCounter(CAM.AnimatorTargets.gameObject, 0, 0.2f);
        //CAM.AnimatorTargets.localRotation = Quaternion.Euler(0, 0, 0);
        //trigger Going out jaccuzi
        yield return new WaitForSeconds(t/2);
        if (!CM.tookTawle)
        {
            
            CC.Targets[2].GetComponent<Bed>().CTT.makeNullObject();
            CC.Targets[2].GetComponent<Bed>().isCustomerNear = false;
            
            CM.tookTawle = true;
        }
        
        yield return new WaitForSeconds(t);
        //FaceTowardCounter(CAM.AnimatorTargets.gameObject, 0, 0.2f);
        //CAM.AnimatorTargets.localRotation = Quaternion.Euler(0, 0, 0);
        CM.isOnBed = false;
    }


    public void exitChangingCloth(float distance)
    {
        if (!CM.ChangedClothOnExit && CM.CustomerProcessCompleted)
        {
            if (CC.Targets[0] && Vector3.Distance(transform.position, CC.Targets[0].position) <= distance)
            {
                if (!CC.agent.isStopped)
                {
                    //jump to change cloth
                    print("Jump Session Started");
                    StartCoroutine(changeCloth(true));
                    CM.ChangingStationOnExit = true;
                    CC.agent.isStopped = true;
                }
            }
        }
    }

    public void MoneyThrow(float distance)
    {
        if (!CM.GaveMoney && CM.ChangedClothOnExit)
        {
           // if (CC.Targets[3]) DistanceTemp = Vector3.Distance(transform.position, CC.Targets[3].position);
            if (CC.Targets[3] && Vector3.Distance(transform.position, CC.Targets[3].position) <= distance)
            {
                
                if (!CC.agent.isStopped)
                {
                    //Money throw started
                    StartCoroutine(ThrowMoney(1f));
                    CC.agent.isStopped = true;
                    CM.CounterService = true;
                }
            }
        }
    }
    IEnumerator ThrowMoney(float t)
    {
        yield return new WaitForSeconds(0.15f);
        CAM.triggerAnimation("Vic");
        //if there any animation trigger
        yield return new WaitForSeconds(t/2);
        //throw money in for loop
        spwanCashMoney();
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.CustomerHappy);
        CM.GaveMoney = true;
        GameManager.instance.customerToServed += 1;
        yield return new WaitForSeconds(t);
        CC.agent.isStopped = false;
    }

    void spwanCashMoney()
    {
        for (int i = 0; i < CashSpwanCount; i++)
        {
            GameObject c = Instantiate(Cash, new Vector3(transform.position.x, transform.position.y + 3, transform.position.z), Quaternion.identity);
            if (i >= CashSpwanCount - 1) c.GetComponent<cash>().isPlay = true;
        }
        
    }

    public void ResetCustomerWhileReachingExitPoint(float distance)
    {
        if (CM.GaveMoney)
        {
           // if(CC.Targets[CC.Targets.Length-1]) DistanceTemp = Vector3.Distance(transform.position, CC.Targets[CC.Targets.Length - 1].position);
            if (CC.Targets[CC.Targets.Length - 1] && Vector3.Distance(transform.position, CC.Targets[CC.Targets.Length - 1].position) <= distance)
            {
                if (!CC.agent.isStopped)
                {
                    ResetEverythingOfCustomer();
                    CC.agent.isStopped=true;

                }
                //Reset Everything
            }
        }
    }
    void ResetEverythingOfCustomer()
    {
        

        for (int i = 0; i < CC.Targets.Length-1; i++)
        {
            CC.Targets[i] = null;
        }
        CM.isClothChanged = false;
        CM.isBedOpen = false;
        CM.SittingArea = false;
        CM.isOnBed = false;
        CM.tookTawle = false;
        CM.CustomerProcessCompleted = false;
        CM.CounterService = false;
        CM.GaveMoney = false;
        CM.ChangingStationOnExit = false;
        CM.ChangedClothOnExit = false;
        CC.agent.isStopped = false;
        isPuff = false;
        transform.parent = CC.Parant;
        isDesp = false;

    }

    public void colliderClose(bool istrue = true)   
    {
        /*if (!istrue) CC.agent.radius = 0;
        if (istrue) CC.agent.radius = 0.45f;*/
    }
    public void switchChar(int i, bool isT = true)
    {
        if (i==0)
        {
            Character_1.SetActive(true);
            Character_2.SetActive(false);
            Character_3.SetActive(false);
            //puff.Play();
            Destroy(Instantiate(puffObj, transform.position + new Vector3(0, 1, 0), Quaternion.identity), 2.5f);
        }
        else if (i==1)
        {
            Character_1.SetActive(false);
            Character_2.SetActive(true);
            Character_3.SetActive(false);
            if (isT)Destroy(Instantiate(puffObj, transform.position + new Vector3(0,1,0), Quaternion.identity), 2.5f);
        }
        else if (i == 2)
        {
            Character_1.SetActive(false);
            Character_2.SetActive(false);
            Character_3.SetActive(true);
        }
    }


}
