using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum target {
    ChangingSlotePosition = 0,
    SofaSeatPosition=1,
    BedPosition=2,
    StoreCounter = 3,
    Exit
}
public class CustomerController : MonoBehaviour
{
    public Transform[] Targets;

    public NavMeshAgent agent;
    public Transform Parant;
    public BedManager bedManager;    
    private CustomerMove move;
    private void Awake()
    {
        move = GetComponent<CustomerMove>();
    }

    private void Update()
    {
        getBedPosition();
    }
    public void moveControl(target target)
    {
        switch (target)
        {
            case target.SofaSeatPosition:
                moveAgent(((int)target.SofaSeatPosition));
                break;
            case target.ChangingSlotePosition:
                moveAgent(((int)target.ChangingSlotePosition));
                break;
            case target.BedPosition:
                moveAgent(((int)target.BedPosition));
                break;
            case target.StoreCounter:
                moveAgent(((int)target.StoreCounter));
                break;
            case target.Exit:
                moveAgent(Targets.Length - 1);
                break;
        }
    }
    void moveAgent(int i)
    {
        if(Targets[i]) agent.SetDestination(Targets[i].position);
    }

    void getBedPosition()
    {
        if (!move.tookTawle && move.isClothChanged && bedManager)
        {
            if (bedManager.beds.Count > 0 && !bedManager.beds[0].isBooked && !Targets[2])
            {
                Targets[2] = bedManager.beds[0].transform;
                bedManager.beds[0].isBooked = true;
                move.isBedOpen = true;
            }
        }         
    }
}
