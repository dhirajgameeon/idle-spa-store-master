using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowleStoolPosition : MonoBehaviour
{
    public StaffAIManager SAM;
    public controlTowleTable CTT;
    public bool isBooked;

    private void Awake()
    {
        SAM = GetComponentInParent<Bed>().SAM;
        CTT = GetComponentInParent<controlTowleTable>();
        CTT.TSP = this;
    }

    private void Update()
    {
        BookingCheck();
    }
    public void BookingCheck()
    {
        if (SAM)
        {
            if(isBooked && SAM.TSP.Contains(this))
            {
                SAM.TSP.Remove(this);
            }
            if (!isBooked && !SAM.TSP.Contains(this))
            {
                SAM.TSP.Add(this);
            }
        }
        if(CTT.Towle.Count > 0)
        {
            isBooked = true;
        }
    }
}
