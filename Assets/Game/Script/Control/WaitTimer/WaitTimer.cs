using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WaitTimer : MonoBehaviour
{
    public Image FIlledImage;
    public float WaitTimerSpeed;
    public float WaitTimerTime;

    public bool isWaitTimerActive;
    public bool isWaitTimerComplete;

    private float fillTimer;

    // Start is called before the first frame update
    void Start()
    {
        fillTimer = WaitTimerTime;
        //FIlledImage.fillAmount = WaitTimerTime;
        FIlledImage.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (isWaitTimerActive && !isWaitTimerComplete) waitTimer();
        transform.forward = -Camera.main.transform.forward;
    }

    public void ResetTimer()
    {
        if (!isWaitTimerComplete)
        {
            FIlledImage.gameObject.SetActive(false);
            isWaitTimerComplete = false;
            isWaitTimerActive = false;
            fillTimer = WaitTimerTime;
        }        
    }


    float slider;
    void waitTimer()
    {
        FIlledImage.gameObject.SetActive(true);

        if (fillTimer > 0) fillTimer -= WaitTimerSpeed * Time.deltaTime;
        slider = fillTimer / WaitTimerTime;
        FIlledImage.fillAmount = slider;
        if(fillTimer <= 0)
        {
            isWaitTimerComplete = true;
            FIlledImage.gameObject.SetActive(false);
            fillTimer = WaitTimerTime;
        }
    }
}
