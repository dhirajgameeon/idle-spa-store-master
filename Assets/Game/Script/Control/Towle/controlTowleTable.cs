using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlTowleTable : MonoBehaviour
{

    public int NeedItemID;
    public List<Transform> Towle = new List<Transform>();
    public bool isTowlePresent;
    public ParticleSystem PoofEffect;
    public Transform inventory;

    public TowleStoolPosition TSP;
    void Update()
    {
        checkForTowle();
    }

    
    public void makeNullObject()
    {
        PoofEffect.Play();
        foreach (Transform item in Towle.ToArray())
        {
            item.GetComponent<controlItem>().ResetTowle();
        }
        Towle.Clear();
        isTowlePresent = false;
        if(TSP && TSP.isBooked) TSP.isBooked = false;
    }
    void checkForTowle()
    {
        foreach (Transform item in inventory)
        {
            if (!Towle.Contains(item))
            {
                Towle.Insert(0, item);
            }
        }
        foreach (Transform item in Towle.ToArray())
        {
            if(item == null)
            {
                Towle.Remove(item);
            }
        }

       /* if (Towle.Count <= 0)
        {
            isTowlePresent = false;
        }
        if (Towle.Count > 0)
        {
            isTowlePresent = true;
        }*/
    }
}
