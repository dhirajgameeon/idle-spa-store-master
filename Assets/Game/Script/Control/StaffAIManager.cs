using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffAIManager : MonoBehaviour
{
    public List<TowleStoolPosition>TSP = new List<TowleStoolPosition>();
    public List<TowleTablePosition>TTP = new List<TowleTablePosition>();
    public List<Bed> Bed = new List<Bed>();



    //Staff management
    [Header("Employee Hiring System")]
    public List<GameObject> Employee = new List<GameObject>();
    public List<Transform> EmployeeInitialPosition = new List<Transform>();

    public HireEmployee HE;
    public GameObject HireUI;
    public Transform DustibinPosition;

    public int StaffPrice = 100;
    public int maxNumberOfHiring;
    private void Start()
    {
    }
    private void Update()
    {
        //OpenUI();
    }

    public void closeUI()
    {
        HireUI.GetComponent<Animator>().Play("Exit");
        HireUI.GetComponent<HireUI>().hireEmployee = null;
        HE.isClosed = true;
    }
    public void Hire(int staffPrice)
    {
        if(GameManager.instance.maxCash >= staffPrice && maxNumberOfHiring < 3)
        {
            GameObject emp = Employee[0];
            Transform pos = EmployeeInitialPosition[Random.Range(0, EmployeeInitialPosition.Count - 1)];
            EmployeeInitialPosition.Remove(pos);
            Employee.Remove(emp);
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.HIreStaffButton);
            emp.transform.position = pos.position;
            emp.SetActive(true);
            emp.GetComponent<moveEmp>().InitalPosition = pos;
            emp.GetComponent<moveEmp>().TargetToDustbin = DustibinPosition;
            emp.GetComponent<moveEmp>().SAM = this;
            GameManager.instance.maxCash -= StaffPrice;
            GameManager.instance.unlockedEmployee += 1;
            maxNumberOfHiring += 1;
        }
    }



}
