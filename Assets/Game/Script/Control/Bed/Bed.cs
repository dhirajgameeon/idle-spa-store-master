using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bed : MonoBehaviour
{
    public int BedID = 0;

    public WaitTimer waitTimer;
    public controlTowleTable CTT;
    public Transform bathPosition;
    public Transform PP;

    public bool isBooked;
    public bool isBookedByAI;
    public bool isUnlocked = false;

    public bool isPlayerNear = false;
    public bool isCustomerNear = false;
    public float StartMassageTimer = 1;
    public StaffAIManager SAM;
    private void Update()
    {
        if(BedID == 1)customerService();
        if (BedID == 1) bedAvailabity();
    }

    float StartWaitTimer = 1;
    public void customerService()
    {
        if(!isCustomerNear || !isPlayerNear)
        {
            //call for player
            //resetTimer
            waitTimer.ResetTimer();
            StartWaitTimer = StartMassageTimer;
        }
        if(isCustomerNear && isPlayerNear)
        {
            if (StartWaitTimer > 0) StartWaitTimer -= Time.deltaTime;
            if (!waitTimer.isWaitTimerActive && StartWaitTimer <= 0)
            {
                //customer animation player
                waitTimer.isWaitTimerActive = true;
                StartWaitTimer = StartMassageTimer;
            }
            
        }
    }

    public void bedAvailabity()
    {
        if(isCustomerNear && !isPlayerNear && !isBookedByAI)
        {
            if(!SAM.Bed.Contains(this))SAM.Bed.Add(this);
        }
        if(isBookedByAI || !isCustomerNear)
        {
            if (SAM.Bed.Contains(this)) SAM.Bed.Remove(this);
        }

    }
    
    
    
    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            try
            {
                if (other.gameObject.GetComponent<movePlayer>().direction.magnitude < 0.1f)
                {
                    isPlayerNear = true;
                }
                if (other.gameObject.GetComponent<movePlayer>().direction.magnitude > 0)
                {
                    isPlayerNear = false;
                }
            }
            catch
            {

            }
            
        }
        if (other.CompareTag("emp"))
        {
            try
            {
                if (other.gameObject.GetComponent<moveEmp>().agent.velocity.magnitude < 0.1f)
                {
                    isPlayerNear = true;
                }
                if (other.gameObject.GetComponent<moveEmp>().agent.velocity.magnitude > 0)
                {
                    isPlayerNear = false;
                }
            }
            catch
            {

            }

        }
    }



    private void OnTriggerExit(Collider other)
    {
        if (isPlayerNear) isPlayerNear = false;
    }


}
