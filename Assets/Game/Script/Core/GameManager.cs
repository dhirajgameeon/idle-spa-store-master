using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

/// <summary>
 /// Cash Counter
 /// </summary>

    [Header("Cash Counter")]
    public float maxCash;
    public float cashCounterSpeed;
    private float currentCash;
    public float leanTweeningTime = 0.25f;
    public TextMeshProUGUI cashUI;
    public GameObject CashIcon;
    public GameObject CountIcon;
    
    /// <summary>
    /// Progress Slider
    /// </summary>
    [Header("Slider")]
    [Space(10)]
    public Slider customerCount;
    public Image fillImage;
    public float USpeed;
    public float DSpeed;
    public int customerToServe;
    public int customerToServed;
    public int spawnnedCustomers = 0;

    [Header("Customer UI")]
    [Space(5)]
    public List<Sprite> CustomerUI = new List<Sprite>();
    public List<Sprite> customEmoji = new List<Sprite>();

    [Header("SectionUnlock")]
    [Space(5)]
    public TMP_Text sectionManager;
    public List<SectionManager> section = new List<SectionManager> ();
    public int UnlockedSectionCount = 0;

    [Header("Employee")]
    [Space(5)]
    public TMP_Text employeeCounter;
    public int unlockedEmployee = 0;

    [Header("Game Tutorial")]
    [Space(0)]
    public bool isSpwanCash = true;
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        currentCash = maxCash;
        BGMplay();
    }

    float currentProgress;
    void Update()
    {
        cashUI.text = cashCount().ToString("N0");
        customerCount.value = progressSlider(customerToServed);
        sectionCountManager();
        shopCountManager();

        if (Input.GetMouseButtonDown(1)) cashIconAnimation();
    }
    
    public void cashIconAnimation()
    {
        resetIconSize();
        LeanTween.scale(CashIcon, new Vector3(1.3f, 1.3f, 1.3f), leanTweeningTime).setEasePunch();        
        LeanTween.scale(CountIcon, new Vector3(1.3f, 1.3f, 1.3f), leanTweeningTime).setEasePunch();        
    }
public void resetIconSize()
    {
        CashIcon.transform.localScale = new Vector3(1,1,1);
        CountIcon.transform.localScale = new Vector3(1, 1, 1);
    }
    float cashCount()
    {
        if (currentCash <= maxCash)
        {
            currentCash += cashCounterSpeed * Time.deltaTime;
            if (currentCash >= maxCash) currentCash = maxCash;
        }
        if (currentCash >= maxCash)
        {
            currentCash -= cashCounterSpeed * Time.deltaTime;
            if (currentCash <= maxCash) currentCash = maxCash;
        }
        if (currentCash < 0.9f || maxCash < 0.9f)
        {
            currentCash = 0;
            maxCash = 0;
        }
        currentCash = Mathf.Clamp(currentCash, 0, Mathf.Infinity);
        return currentCash;

    }

    void BGMplay()
    {
        EvenManager.TriggerBGMPlayEvent(AudioID.BGM);
    }
    void sectionCountManager()
    {
        sectionManager.text = (UnlockedSectionCount + 1) + "/" + section.Count;
    }
    void shopCountManager()
    {
        employeeCounter.text = unlockedEmployee.ToString("N0");
    }

    float progressSlider(float maxProgress)
    {
        if (currentProgress < maxProgress)
        {
            currentProgress += USpeed * Time.deltaTime;
            if (currentProgress >= maxProgress) currentProgress = maxProgress;
        }
        if (currentProgress > maxProgress)
        {
            currentProgress -= DSpeed * Time.deltaTime;
            if (currentProgress <= maxProgress) currentProgress = maxProgress;
        }
        if (currentProgress == maxProgress)
            currentProgress = maxProgress;

        return currentProgress;
    }
}
