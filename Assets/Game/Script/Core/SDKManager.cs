using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;
using GameAnalyticsSDK;

public class SDKManager : MonoBehaviour
{
    private void Awake()
    {
        FB.Init();
        GameAnalytics.Initialize();
    }
}
