using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;


[System.Serializable]
public class BoolGameEvent : UnityEvent<bool> { }

[System.Serializable]
public class SFXOneShotPlayEvent : UnityEvent<AudioID, UnityAction> { }
public class BGMPlayEvent : UnityEvent<AudioID> { }


public partial class EvenManager : MonoBehaviour
{

    private static BGMPlayEvent BGMPlayEvent = new BGMPlayEvent();
    public static void AddBGMPlayEvent(UnityAction<AudioID> action)
    {
        BGMPlayEvent.AddListener(action);
    }

    public static void RemoveBGMPlayEvent(UnityAction<AudioID> action)
    {
        BGMPlayEvent.RemoveListener(action);
    }

    public static void TriggerBGMPlayEvent(AudioID audioID)
    {
        BGMPlayEvent.Invoke(audioID);
    }



    /// </summary>
    /// 
    private static SFXOneShotPlayEvent SFXOneShotPlayEvent = new SFXOneShotPlayEvent();

    public static void AddSFXOneShotPlayEvent(UnityAction<AudioID, UnityAction> action)
    {
        SFXOneShotPlayEvent.AddListener(action);
    }

    public static void RemoveSFXOneShotPlayEvent(UnityAction<AudioID, UnityAction> action)
    {
        SFXOneShotPlayEvent.RemoveListener(action);
    }

    public static void TriggerSFXOneShotPlayEvent(AudioID audioID, UnityAction act = null)
    {
        SFXOneShotPlayEvent.Invoke(audioID, act);
    }
    ///
    ///
}
