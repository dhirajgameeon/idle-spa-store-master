using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class SectionManager : MonoBehaviour
{
    [Header("Unlocking System")]
    public sectionUnlocker unlocker;
    public bool isLock=false;

    [Space(10)]
    public List<GameObject> CSP = new List<GameObject>();
    public List<Shop>ListOfShop = new List<Shop>();
    public List<GameObject> Customer = new List<GameObject>();

    [Space(10)]
    public Transform customerList;
    public Transform ExitPosition;

    [Header("Call Customer Veriables")]
    public float spwanTimer = 2;
    float spwaner;

    private void Start()
    {
        unlock();
    }
    void Update()
    {
        checkForSectionUnlock();

        if (customerList)addCustomerToList();
        clearCustomerList();
        if(LevelManager.instance.isSpawnActive || LevelManager.instance.Level > 1)CallCustomer();
    }

    void unlock()
    {
        if(!unlocker.isLocked && LevelManager.instance.Level >= unlocker.UnlockingLevel)
        {
            if(unlocker.Unlocked)unlocker.Unlocked.SetActive(true);
            if (unlocker.Locked) unlocker.Locked.SetActive(false);
            if(!isLock) GameManager.instance.UnlockedSectionCount++;
        }
    }

    void checkForSectionUnlock()
    {
        if (unlocker.isLocked && LevelManager.instance.Level >= unlocker.UnlockingLevel)
        {
            Invoke(nameof(setActive), unlocker.unlockDelay);
            if(unlocker.stateName.Length > 0) unlocker.cinemachineCamera.Play(unlocker.stateName);            
            GameManager.instance.UnlockedSectionCount++;
            unlocker.isLocked = false;
        }
    }

    void setActive()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.SectionUnlock);
        unlocker.Unlocked.SetActive(true);
        unlocker.Locked.SetActive(false);
        unlocker.unlockPartical.Play();
    }

    void CallCustomer()
    {
        if(CSP.Count > 0 && Customer.Count > 0)
        {
            if (spwaner > 0) spwaner -= Time.deltaTime;
            if (spwaner <= 0)
            {
                GameObject customer = Customer[Random.Range(0, Customer.Count - 1)];
                customer.SetActive(true);
                customer.transform.parent = null;
                customer.transform.position = customerList.transform.position;
                customer.GetComponent<moveCustomer>().sectionManager = GetComponent<SectionManager>();
                if(ExitPosition)customer.GetComponent<moveCustomer>().end = ExitPosition;
                GameManager.instance.spawnnedCustomers += 1;
                spwaner = spwanTimer;
                return;
            }
        }
    }
    void addCustomerToList()
    {
        foreach (Transform item in customerList)
        {
            if (!Customer.Contains(item.gameObject))
            {
                item.gameObject.SetActive(false);
                Customer.Add(item.gameObject);
            }
            if(item.gameObject.activeSelf) item.gameObject.SetActive(false);
        }
    }
    void clearCustomerList()
    {
        if (Customer.Count > 0)
        {
            for (int i = 0; i < Customer.Count - 1; i++)
            {
                if (Customer[i].activeSelf)
                {
                    Customer.Remove(Customer[i]);
                }
            }
        }
    }
}
