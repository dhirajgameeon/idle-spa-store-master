using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaUnlocker : MonoBehaviour
{
    public sectionUnlocker UnlockValues;
    public bool isLock = false;

    private void Start()
    {
        Unlock();
    }
    private void Update()
    {
        checkForSectionUnlock();
    }


    public void checkForSectionUnlock()
    {
        if (UnlockValues.isLocked && LevelManager.instance.Level >= UnlockValues.UnlockingLevel)
        {
            Invoke(nameof(setActive), UnlockValues.unlockDelay);
            if (UnlockValues.stateName.Length > 0) UnlockValues.cinemachineCamera.Play(UnlockValues.stateName);
            GameManager.instance.UnlockedSectionCount++;
            UnlockValues.isLocked = false;
        }
    }
    void setActive()
    {
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.SectionUnlock);
        UnlockValues.Unlocked.SetActive(true);
        UnlockValues.Locked.SetActive(false);
        UnlockValues.unlockPartical.Play();
    }

    void Unlock()
    {
        if (!UnlockValues.isLocked && LevelManager.instance.Level >= UnlockValues.UnlockingLevel)
        {
            if (UnlockValues.Unlocked) UnlockValues.Unlocked.SetActive(true);
            if (UnlockValues.Locked) UnlockValues.Locked.SetActive(false);
            if (!isLock) GameManager.instance.UnlockedSectionCount++;
        }
    }
}
