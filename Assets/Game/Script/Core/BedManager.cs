using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BedManager : MonoBehaviour
{
    public List<Bed> beds = new List<Bed>();
    public Transform BedList;

    private void Update()
    {
        checkforBeds();
    }
    void checkforBeds()
    {
        foreach (Transform item in BedList)
        {
            if ( item.gameObject.activeSelf && item.GetComponent<Bed>().isUnlocked)
            {
                if (item.GetComponent<Bed>().isBooked && beds.Contains(item.GetComponent<Bed>()))
                {
                    beds.Remove(item.GetComponent<Bed>());
                }
                if (!item.GetComponent<Bed>().isBooked && !beds.Contains(item.GetComponent<Bed>()))
                {
                    beds.Add(item.GetComponent<Bed>());
                }
            }
        }
    }

}
