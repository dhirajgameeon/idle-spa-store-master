using UnityEngine;

public enum AnimationState {
Idle,
Run,
Death,
Victory
}
public class animationHandler : MonoBehaviour
{
    public Animator anim;

    public void playAnim(AnimationState state, float speed=1, bool loop=true)
    {
        switch (state)
        {
            case AnimationState.Idle:
                IdleAnimation();
                break;
            case AnimationState.Run:
                RunAnimation(speed);
                break;
            case AnimationState.Death:
                DeathAnimation();
                break;
            case AnimationState.Victory:
                victory();
                break;
        }
    }

    void IdleAnimation()
    {
        anim.Play("Idle");
    }
    void RunAnimation(float speed)
    {
        anim.SetFloat("speed", speed);
    }
    void DeathAnimation()
    {
        anim.Play("Death");
    }
    void victory()
    {
        anim.SetTrigger("victory");
    }
}
