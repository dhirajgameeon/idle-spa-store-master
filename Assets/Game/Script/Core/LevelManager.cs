using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;
public class LevelManager : MonoBehaviour
{
    public static LevelManager instance;

    public int Level;
    public TMP_Text currentLevelText;
    public TMP_Text nextLevelText;
    public GameObject NextLevelButton;
    public ParticleSystem levelUp;
    public Animator levelUpgradeUI;
    public bool isLevelUp = false;

    [Header("Level Manager")]
    public List<int> CustomerToServeList = new List<int>();
    public List<int> Reward = new List<int>();

    private GameManager gameManager;
    int data = 0;
    // Start is called before the first frame update
    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        gameManager = GameManager.instance;
        currentLevelText.text = (Level + 1).ToString("N0");
        nextLevelText.text = (Level + 2).ToString("N0");
        gameManager.customerCount.maxValue = MaxCustomerCount(data);
        gameManager.customerToServe = MaxCustomerCount(data);
        customerServed = false;
    }

    private void Update()
    {
        activeUpdateButton();
        LevelUpgrade();
    }

    public bool isSpawnActive = true;
    bool fillImageSlider;
    bool customerServed = false;
    float x = 0;
    void activeUpdateButton()
    {
        if (isSpawnActive)
        {            
            if (gameManager.spawnnedCustomers >= gameManager.customerToServe)
            {
                isSpawnActive = false;                
            }
        }
        if (!fillImageSlider && !customerServed)
        {
            if (gameManager.customerToServed >= gameManager.customerToServe)
            {
                NextLevelButton.SetActive(true);
                EvenManager.TriggerSFXOneShotPlayEvent(AudioID.LevelUp);
                fillImageSlider = true;
                customerServed = true;
            }
        }

        if (fillImageSlider)
        {
            if (x <= 1) x += 10 * Time.deltaTime;
            gameManager.fillImage.fillAmount = x;
            if (x >= 1)
            {
                x = 0;
                fillImageSlider = false;
            }
        }
    }
    
    void LevelUpgrade()
    {
        if (NextLevelButton.activeSelf && !isLevelUp)
        {
            levelUpgradeUI.Play("pop");
            StartCoroutine(levelUpDelay(1));
            isLevelUp = true;
            NextLevelButton.SetActive(false);
        }
    }



    IEnumerator levelUpDelay(float t)
    {
        yield return new WaitForSeconds(t);
        NextLevel();
    }

    public void NextLevel()
    {

        Level++;        
        currentLevelText.text = (Level + 1).ToString("N0");
        nextLevelText.text = (Level + 2).ToString("N0");
        fillImageSlider = false;
        customerServed = false;
        gameManager.fillImage.fillAmount = 0;
        customerManager();
        AddRewardAmount();
        callOnce();
        isLevelUp = false;
    }

    void customerManager()
    {
        gameManager.customerToServed -= gameManager.customerToServe;
        gameManager.customerToServe = MaxCustomerCount(data);
        gameManager.customerCount.maxValue = MaxCustomerCount(data);
        gameManager.spawnnedCustomers = 0;
    }
    void callOnce()
    {
        levelUp.Play();
        gameManager.cashIconAnimation();
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.LevelUpClick);
    }
    void AddRewardAmount()
    {
        int Data = 0;
        gameManager.maxCash += RewardAmount(Data);
    }
    public int MaxCustomerCount(int customerData)
    {
        if (Level < 20) customerData = CustomerToServeList[Level];
        if (Level >= 21) customerData = 100;
        return customerData;
    }
    public int RewardAmount(int amount)
    {
        if (Level < 20) amount = Reward[Level];
        if (Level >= 21) amount = 1000;
        return amount;
    }
}
