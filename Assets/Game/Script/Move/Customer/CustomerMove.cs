using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomerMove : MonoBehaviour
{
    private CustomerController controller;

    //Where to go
    public bool isClothChanged;
    public bool isBedOpen;

    public bool SittingArea;

    public bool isOnBed;

    public bool tookTawle;
    public bool CustomerProcessCompleted;

    public bool CounterService;
    public bool GaveMoney;

    public bool ChangingStationOnExit;
    public bool ChangedClothOnExit;

    //Current Status
    public bool isSitting;
    
    //Rotation
    public float rotationSmooth;
    
    
    private float turnSmoothVelocity;

    private void Awake()
    {
        controller = GetComponent<CustomerController>();
    }

    private void Update()
    {
        WhereToMove();
        customerRotation();
    }

    void WhereToMove()
    {
        if (!isClothChanged)
        {
            controller.moveControl(target.ChangingSlotePosition);
        }
        if (isClothChanged && !GaveMoney)
        {
            if (isBedOpen && !CustomerProcessCompleted)
            {
                controller.moveControl(target.BedPosition);
            }
            if (!isBedOpen && !CustomerProcessCompleted)
            {
                if (!isSitting)
                {
                    controller.moveControl(target.SofaSeatPosition);
                }
            }
            if (CustomerProcessCompleted && !ChangingStationOnExit)
            {
                controller.moveControl(target.ChangingSlotePosition);
            }
            if(ChangingStationOnExit && ChangedClothOnExit)
            {
                controller.moveControl(target.StoreCounter);
            }           
        }
        if (GaveMoney)
        {
            controller.moveControl(target.Exit);
        }
    }

    void customerRotation()
    {
        if (controller.agent.velocity.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(controller.agent.velocity.x, controller.agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }

}
