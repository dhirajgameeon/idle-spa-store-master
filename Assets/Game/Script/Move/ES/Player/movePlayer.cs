using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;


    public class movePlayer : MonoBehaviour
    {
    public Joystick Joystick;
    public Transform groundC;
    public Vector3 movementDirection = new Vector3(1, 1, 1);
    public NavMeshObstacle NMOO;
    [Range(0, 360)]
    public float angleForIsometricCamera = 0;
    public float rotationSmooth;
    public float gravityForce = 9.8f;
    public float MovementSpeed = 10;

    [HideInInspector] public Vector3 direction;

    private CharacterController CharacterController;
    private float turnSmoothVelocity;
    private controlPlayer controlPlayer;
    private controlStack stack;

    void Start()
    {
        CharacterController = GetComponent<CharacterController>();
        controlPlayer = GetComponent<controlPlayer>();
        stack = GetComponent<controlStack>();
    }

    void Update()
    {
        if (!controlPlayer.isDied) MovePlayer();
        HoldingAnimationTrigger();
        NMO();
    }

    void NMO()
    {
        if(direction.magnitude < 0.1f)
        {
            NMOO.enabled = true;
        }
        if (direction.magnitude >0)
        {
            NMOO.enabled = false;
        }
    }
    public void MovePlayer()
    {
        float x = Joystick.Horizontal;
        float z = Joystick.Vertical;

        direction = new Vector3(x, 0, z).normalized;

        if (direction.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle + angleForIsometricCamera, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
            Vector3 moveDir = Quaternion.Euler(0, targetAngle, 0) * movementDirection;

            CharacterController.Move(moveDir.normalized * MovementSpeed * Time.deltaTime);
        }
        CharacterController.Move(-transform.up * gravityForce * Time.deltaTime);
        controlPlayer.aPlayer.playAnim(AnimationState.Run, direction.magnitude);
    }
    void HoldingAnimationTrigger()
    {
        if (stack.Cart.Count > 0)
        {
            controlPlayer.aPlayer.anim.SetBool("hold", true);
        }
        if (stack.Cart.Count <= 0)
        {
            controlPlayer.aPlayer.anim.SetBool("hold", false);
        }
    }
}
