using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class moveEmp : MonoBehaviour
{
    public int EMP_ID = 0;

    [Header("Targets Positions")]
    public Transform InitalPosition;

    public Transform TargetToTowleTable;
    public Transform TargetToTowleStool;
    public Transform TargetToMassageBed;
    public Transform MassagerPosition;

    public Transform TargetToDustbin;

    [Header("Current State")]
    [Space(5)]
    public bool isWalkingToTable;
    public bool isWalkingToDustbin;
    public bool isWalkingToStool;
    public bool isWalkingToMassageBed;
    public bool isWalkedToInitialPosition;

    [Header("Controller")]
    [Space(5)]
 

    public float rotationSmooth = 0.2f;
    private float turnSmoothVelocity;

    public StaffAIManager SAM;
    public Bed BED;
    /*public Shop shop;*/
    public NavMeshAgent agent;
    public NavMeshObstacle NMO;
    private animationHandler anim;
    private controlEmp controlEmp;

    private void Start()
    {
        anim = GetComponent<animationHandler>();
        controlEmp = GetComponent<controlEmp>();
        agent = GetComponent<NavMeshAgent>();
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.StaffSpawn);
    }

    private void Update()
    {
        mover();
        whereToMove();
        getCustomerLocation();
        HoldAnimationTrigger();
        triggerRotation();
        if(EMP_ID == 1)BedPosition();
        //NMOAgent();
    }

    void NMOAgent()
    {
        if (agent.velocity.magnitude > 0) NMO.enabled = false;
        if (agent.velocity.magnitude < 0.1f) NMO.enabled = true;
    }
    void mover()
    {
        if (agent.velocity.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
        anim.playAnim(AnimationState.Run, agent.velocity.magnitude);
    }
    void HoldAnimationTrigger()
    {
        if (controlEmp.Cart.Count > 0) anim.anim.SetBool("hold", true);
        if (controlEmp.Cart.Count <= 0) anim.anim.SetBool("hold", false);
    }
    void getCustomerLocation()
    {
        if (SAM)
        {
            if(SAM.TSP.Count > 0 && !TargetToTowleStool)
            {
                TargetToTowleStool = SAM.TSP[0].transform;
                SAM.TSP[0].isBooked = true;
            }
            if (controlEmp.Cart.Count <= 0 && !TargetToTowleTable && TargetToTowleStool)
            {
                print("<color=red>Task Passed</color>");
                if(SAM.TTP.Count > 0)
                {
                    TargetToTowleTable = SAM.TTP[0].transform;
                    SAM.TTP[0].isBooked=true;
                }
            }
        }
        if (TargetToTowleStool)
        {
            if (TargetToTowleStool.GetComponent<TowleStoolPosition>().CTT.isTowlePresent)
            {
                TargetToTowleStool = null;
                if (!isWalkingToDustbin)
                {
                    isWalkingToStool = false;
                    isWalkingToDustbin = true;
                }
            }
        }



        if (controlEmp.Cart.Count > 0 && TargetToTowleTable)
        {
            TargetToTowleTable.GetComponent<TowleTablePosition>().isBooked = false;
            TargetToTowleTable = null;
        }
        




            /*  if (!TargetToTowleStool && shop.CustomerOnStore.Count > 0)
              {
                  if (shop.CustomerOnStore[0].isStartTrade && !shop.CustomerOnStore[0].control.isItemTookFromPlayer)
                  {
                      TargetToTowleStool = shop.CustomerOnStore[0].transform;
                  }
              }
              if (TargetToTowleStool)
              {
                  if (TargetToTowleStool.GetComponent<controlCustomer>().isItemTookFromPlayer)
                  {
                      TargetToTowleStool = null;
                  }
              }*/
        }
    void whereToMove()
    {
        if(!MassagerPosition && TargetToTowleStool && controlEmp.Cart.Count <= 0 && !isWalkingToTable)
        {            
            isWalkingToTable = true;
            isWalkingToStool =false;
            isWalkingToDustbin=false;
            isWalkingToMassageBed = false;
        }
        if (TargetToTowleStool && controlEmp.Cart.Count > 0 && !isWalkingToStool)
        {
            isWalkingToDustbin =false;
            isWalkingToStool = true;
            isWalkingToTable = false;
            isWalkingToMassageBed = false;
        }
        if (!TargetToTowleStool && controlEmp.Cart.Count > 0 && !isWalkingToDustbin)
        {
            isWalkingToDustbin = true;
            isWalkingToStool = false;
            isWalkingToTable = false;
            isWalkingToMassageBed = false;
        }
        if (!TargetToTowleStool && controlEmp.Cart.Count <= 0)
        {
            isWalkingToDustbin = false;
            isWalkingToStool = false;
            isWalkingToTable = false;
            isWalkingToMassageBed = false;


        }
        if(MassagerPosition && controlEmp.Cart.Count <= 0)
        {
            isWalkingToDustbin = false;
            isWalkingToStool = false;
            isWalkingToTable = false;
            isWalkingToMassageBed = true;
        }

        if (!isWalkingToStool && !isWalkingToDustbin && !isWalkingToDustbin && !MassagerPosition) agent.SetDestination(InitalPosition.position);

        if (isWalkingToStool) agent.SetDestination(TargetToTowleStool.position);
        if (isWalkingToDustbin) agent.SetDestination(TargetToDustbin.position);
        if (isWalkingToTable) agent.SetDestination(TargetToTowleTable.position);
        if (isWalkingToMassageBed) agent.SetDestination(MassagerPosition.position);
    }
    void triggerRotation()
    {
        if (Vector3.Distance(transform.position, InitalPosition.position) <= 0.3f && !isWalkedToInitialPosition)
        {
            FaceTowardCounter(InitalPosition.eulerAngles.y, 0.2f);
             isWalkedToInitialPosition =true;
        }
        if (Vector3.Distance(transform.position, InitalPosition.position) > 0.3f && isWalkedToInitialPosition)
        {
            isWalkedToInitialPosition = false;
        }
    }
    void BedPosition()
    {
        if (SAM.Bed.Count > 0)
        {
            MassagerPosition = SAM.Bed[0].PP;
            BED = SAM.Bed[0];
            SAM.Bed[0].isBookedByAI = true;
        }
        if(MassagerPosition && BED && BED.waitTimer.isWaitTimerComplete)
        {
            BED.isBookedByAI = false;
            MassagerPosition = null;
            BED = null;
        }
        if(MassagerPosition && BED && !BED.waitTimer.isWaitTimerComplete && !BED.isCustomerNear)
        {
            BED.isBookedByAI = false;
            MassagerPosition = null;
            BED = null;
        }
        if (MassagerPosition && BED && !BED.waitTimer.isWaitTimerComplete && BED.isPlayerNear)
        {
            if(Vector3.Distance(transform.position, MassagerPosition.position) > 0.5f)
            {
                BED.isBookedByAI = false;
                MassagerPosition = null;
                BED = null;
            }
           
        }
    }
    public void ResetStuffAfterGiving()
    {
        TargetToTowleStool = null;
    }
    void storeTrue()
    {
        isWalkingToTable = true;
    }
    void FaceTowardCounter(float direction, float time)
    {
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }
}
