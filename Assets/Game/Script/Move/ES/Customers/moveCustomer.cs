using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class moveCustomer : MonoBehaviour
{
    public NavMeshAgent agent;
    public float Rotate;
    public Transform target;
    public Transform end;
    public float rotationSmooth = 0.01f;
    public float lookRotation = 90;

    public bool isRechedCounterPosition = false;
    public bool isStartTrade = false;
    public bool isTradeComplete = false;

    private float turnSmoothVelocity;
    public SectionManager sectionManager;
    private animationHandler anim;
    [HideInInspector]public Collider boxCollider;
    [HideInInspector]public controlCustomer control;


    [HideInInspector] public customerSpeechBubble CSB;
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<animationHandler>();
        control = GetComponent<controlCustomer>();
        boxCollider = GetComponent<Collider>();
    }

    // Update is called once per frame
    void Update()
    {
        findTarget();
        Aniamtion();
        if (target)mover();
        if (end) ResetCheck();
    }


    void Aniamtion()
    {
        anim.playAnim(AnimationState.Run, agent.velocity.magnitude);
    }

    void ResetCheck()
    {
        if (isTradeComplete && Vector3.Distance(transform.position, end.position) < 0.2f)
        {
            //Reset Everything
            resetMovingSystem();
            control.ResetControlSystem();
            isTradeComplete = false;
        }
    }
    void mover()
    {
        if (agent.velocity.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }

        if (!isTradeComplete)
        {
            agent.SetDestination(target.position);
        }

        if (isTradeComplete)
        {
            agent.SetDestination(end.position);
        }

        if (Vector3.Distance(transform.position, target.position) < 0.2f && !isRechedCounterPosition)
        {
            Invoke("callOnReched", 0.1f);
            Invoke("startTradeWithEmp", 0.25f);
            FaceTowardCounter(Rotate, 0.2f);
            isRechedCounterPosition = true;
            boxCollider.enabled = true;
        }
        else if(Vector3.Distance(transform.position, target.position) > 0.2f && isRechedCounterPosition)
        {
            isRechedCounterPosition = false;
        }
    }

    void callOnReched()
    {
        if (CSB)
        {
            print("<color=green>Customer asked for Item</color>");
            CSB.EnableUI();
        }
    }
    void startTradeWithEmp()
    {
        isStartTrade = true;
    }
    void findTarget()
    {
        if (!target && sectionManager.CSP.Count > 0 && !control.isTradeRunning)
        {
            if (!sectionManager.CSP[0].GetComponent<controlStandingPosition>().isOccupied)
            {
                target = sectionManager.CSP[0].transform;
                Rotate = sectionManager.CSP[0].transform.eulerAngles.y/*GetComponent<controlStandingPosition>().rotationFace*/;
                sectionManager.CSP[0].GetComponent<controlStandingPosition>().isOccupied = true;
                control.NeedItemID = sectionManager.CSP[0].GetComponent<controlStandingPosition>().ID;                
                if (!target.GetComponent<controlStandingPosition>().shop.CustomerOnStore.Contains(this) && !control.isItemTookFromPlayer)
                    target.GetComponent<controlStandingPosition>().shop.CustomerOnStore.Add(this);
                return;
            }
        }
    }
    public void resetTarget()
    {
        target.GetComponent<controlStandingPosition>().isOccupied = false;
      //  target = null;
    }
    void resetMovingSystem()
    {
        target = null;
        end = null;
        isStartTrade = false;
        isRechedCounterPosition = false;
        isTradeComplete = false;
        transform.parent = sectionManager.customerList;
        sectionManager = null;
    }

    public void resetCustomerList()
    {
        if (target.GetComponent<controlStandingPosition>().shop.CustomerOnStore.Contains(this))
            target.GetComponent<controlStandingPosition>().shop.CustomerOnStore.Remove(this);
    }
/*    void smoothLookat(Transform target, float rotationSpeed)
    {
        var targetPos = target.position;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }*/
    void FaceTowardCounter(float direction, float time)
    {        
        LeanTween.rotate(this.gameObject, new Vector3(0, direction, 0), time);
    }
}
