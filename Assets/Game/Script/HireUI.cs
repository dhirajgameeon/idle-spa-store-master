using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HireUI : MonoBehaviour
{
    public StaffAIManager hireEmployee;
   // public TextMeshProUGUI PriceTag;
    //public TextMeshProUGUI TotalEmployee;
    private void Start()
    {
        
    }

    private void Update()
    {
        /*if (hireEmployee)
        {
            PriceTag.text = hireEmployee.StaffPrice.ToString("N0");
        }*/
            //TotalEmployee.text = GameManager.instance.unlockedEmployee.ToString("N0");
    }


    public void HireEmployee(int price)
    {        
        hireEmployee.Hire(price);
    }

    public void chageUI(Button b, TextMeshProUGUI t)
    {
        b.interactable = false;
        t.text = "";
    }

    public void CloseUI()
    {
        hireEmployee.closeUI();
    }
    public void SetActive()
    {
        gameObject.SetActive(false);
    }
}
